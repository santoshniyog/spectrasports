-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 07, 2020 at 07:15 AM
-- Server version: 10.4.14-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u291565868_spectra`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_all_services`
--

CREATE TABLE `tbl_all_services` (
  `id` int(15) NOT NULL,
  `image` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `short_description` varchar(6500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_all_services`
--

INSERT INTO `tbl_all_services` (`id`, `image`, `name`, `short_description`, `description`, `date_time`, `status`) VALUES
(1, 'scouting-service.png', 'Scouting', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', '2020-07-05 15:46:41', 0),
(2, 'talent-development-service.png', 'Talent Development', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', '2020-07-05 15:46:41', 0),
(3, 'career-development-service.png', 'Career Development', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', '2020-07-05 15:46:46', 0),
(4, 'scouting-service.png', 'Fund Raising', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', 'Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip', '2020-07-05 15:46:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cf_category`
--

CREATE TABLE `tbl_cf_category` (
  `id` int(15) NOT NULL,
  `name` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_donors`
--

CREATE TABLE `tbl_donors` (
  `id` int(155) NOT NULL,
  `fund_raiser_id` int(15) NOT NULL,
  `user_id` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `amount` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `address` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `pincode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `phone_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `payment_request_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `payment_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_paid` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `payment_method` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_donors`
--

INSERT INTO `tbl_donors` (`id`, `fund_raiser_id`, `user_id`, `name`, `amount`, `address`, `city`, `state`, `pincode`, `phone_number`, `email`, `payment_request_id`, `payment_id`, `is_paid`, `payment_method`, `date_time`, `status`) VALUES
(1, 4, '2', 'praveen murali', '100', 'No. 77 subraniam, rail nagar road, kadaperi, west tambaram', 'Chennai', 'Tamilnadu', '600045', '6374100133', '', 'bc9ec372732a4af696d36487ecff8013', '', '0', '', '2020-09-04 16:30:13', 0),
(2, 5, '2', 'praveen murali', '100', 'No. 77 subraniam, rail nagar road, kadaperi, west tambaram', 'Chennai', 'Tamilnadu', '600045', '6374100133', '', '406cb0b531d3442da9cf836f974d9343', '', '0', '', '2020-09-04 16:35:28', 0),
(3, 5, '0', 'Praveen', '9', 'No. 77 subraniam, rail nagar road, kadaperi, west tambaram', 'Trichy', 'Tamilnadu', '622021', '9500585175', 'praveen.pravin6@gmail.com', 'f4047723dab84760ab48932bcbe6575a', 'MOJO0905D05Q77909163', '1', 'QR - Order based QR Code', '2020-09-05 04:35:24', 0),
(4, 5, '0', 'rajkumar', '9', 'Chennai', 'Chennai', 'Tamilnadu', '600007', '123456789', 'rajkumar@gmail.com', 'b244f5b733c5489690ce787aa4bf765c', 'MOJO0905F05A77931098', '1', 'CARD - Domestic Regular Debit Card (Visa/Mastercard)', '2020-09-05 08:30:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events`
--

CREATE TABLE `tbl_events` (
  `id` int(15) NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_name` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sport` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking_link` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(355) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_for` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(6500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_image` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-image.jpg',
  `event_status` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_events`
--

INSERT INTO `tbl_events` (`id`, `user_id`, `event_name`, `sport`, `booking_link`, `price`, `price_for`, `event_date`, `location`, `short_description`, `description`, `event_image`, `event_status`, `date_time`, `status`) VALUES
(1, '1', 'Test Event', '1', 'https://google.com', '250', 'per ticket', '2020-07-15', '', 'Test Event', '&lt;p&gt;&lt;strong&gt;Test Event 2&lt;/strong&gt;&lt;/p&gt;', 'spectra5ecebb03af163.jpg', 'approved', '2020-05-27 19:09:55', 0),
(2, '2', 'Test Event 2', '1', 'http://google.co.in', '250', 'per ticket', '2020-07-15', '', 'Test Event 2', '&lt;p&gt;&lt;strong&gt;Test Event 2&lt;/strong&gt;&lt;/p&gt;\r\n', 'spectra5ecebb562cce9.jpg', 'pending', '2020-05-27 19:11:18', 0),
(3, '2', 'Jockey Day', '40', 'https://google.com', '200', 'per ticket', '2020-07-15', 'Chennai', 'Horse race', '&lt;p&gt;&lt;strong&gt;Lorem Ipsum&lt;/strong&gt;&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;\r\n', 'spectra5f284a2a4dcf8.jpg', 'approved', '2020-07-15 05:56:55', 0),
(4, '2', 'Jockey Day', '40', 'https://google.com', '200', 'onwards', '2020-07-15', 'Chennai', 'Horse race', '&lt;p&gt;&lt;strong&gt;Lorem Ipsum&lt;/strong&gt;&amp;nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', '2020040915991916625f51ba6e23ca1.jpg', 'pending', '2020-08-03 17:10:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_friend_request`
--

CREATE TABLE `tbl_friend_request` (
  `id` int(155) NOT NULL,
  `request_from` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_to` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `acceptance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `request_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_friend_request`
--

INSERT INTO `tbl_friend_request` (`id`, `request_from`, `request_to`, `type`, `acceptance`, `request_status`, `date_time`, `status`) VALUES
(1, '2', '7', 'student', 'yes', 'pending', '2020-10-06 07:57:43', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_settings`
--

CREATE TABLE `tbl_general_settings` (
  `id` int(15) NOT NULL,
  `option_name` varchar(1500) NOT NULL,
  `value` varchar(5000) NOT NULL DEFAULT '',
  `value2` varchar(5000) NOT NULL DEFAULT '',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_general_settings`
--

INSERT INTO `tbl_general_settings` (`id`, `option_name`, `value`, `value2`, `date_time`, `status`) VALUES
(1, 'top-header-logo', '2020060315834945645e6235a4ee135', '', '2020-02-28 06:04:54', 0),
(2, 'gst', '18', '', '2020-04-05 05:44:37', 0),
(3, 'shipping_cost', '250', '', '2020-04-05 05:45:17', 0),
(4, 'cash_on_delivery', '1', '', '2020-04-05 05:48:06', 0),
(5, 'online', '1', '', '2020-04-05 05:48:16', 0),
(6, 'guest_checkout', '0', '', '2020-04-05 05:48:26', 0),
(7, 'estimated_delivery_in_days', '5', '', '2020-04-08 15:17:55', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_users`
--

CREATE TABLE `tbl_general_users` (
  `id` int(15) NOT NULL,
  `first_name` varchar(355) NOT NULL,
  `last_name` varchar(355) NOT NULL,
  `phone_number` varchar(355) NOT NULL,
  `password` varchar(355) NOT NULL,
  `short_bio` varchar(1500) NOT NULL DEFAULT '',
  `interested_sport` varchar(900) NOT NULL DEFAULT '',
  `organization_name` varchar(1500) NOT NULL DEFAULT '',
  `date_of_birth` varchar(255) NOT NULL DEFAULT '',
  `gender` varchar(900) NOT NULL DEFAULT '',
  `address` varchar(1500) NOT NULL DEFAULT '',
  `city` varchar(900) NOT NULL DEFAULT '',
  `state` varchar(900) NOT NULL DEFAULT '',
  `pincode` varchar(255) NOT NULL DEFAULT '',
  `otp` varchar(355) NOT NULL DEFAULT '',
  `service` varchar(255) NOT NULL DEFAULT '1',
  `profile_image` varchar(900) NOT NULL DEFAULT 'default-image.jpg',
  `profile_banner_image` varchar(900) NOT NULL DEFAULT 'default-image.jpg',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_general_users`
--

INSERT INTO `tbl_general_users` (`id`, `first_name`, `last_name`, `phone_number`, `password`, `short_bio`, `interested_sport`, `organization_name`, `date_of_birth`, `gender`, `address`, `city`, `state`, `pincode`, `otp`, `service`, `profile_image`, `profile_banner_image`, `date_time`, `status`) VALUES
(1, 'praveen', 'murali', '6374100133', 'd8578edf8458ce06fbc5bb76a58c5ca4', '', '', '', '', '', '', '', '', '', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-04-05 12:57:03', 1),
(2, 'praveen', 'murali', '6374100133', 'e10adc3949ba59abbe56e057f20f883e', 'Gold Medalist - Asian Games 2016', 'Cycling', '', '1993-06-18', 'male', 'No. 77 subraniam, rail nagar road, kadaperi, west tambaram', 'Chennai', 'Tamilnadu', '600045', '', '4', 'spectra5fcdd4d55162b.jpg', 'spectra5f29515a58e6d.jpg', '2020-04-05 13:14:30', 0),
(3, 'Suresh', 'Kumar', '9600103904', 'bc9f4c7d20708e06ee3d33d86064cf5a', '', '', '', '', '', '', '', '', '', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-04-12 07:29:24', 0),
(4, 'Praveen', 'M', '9630215487', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '', '', '', '', '3', 'default-image.jpg', 'default-image.jpg', '2020-05-19 22:15:49', 0),
(5, 'Vino', 'Vino', '7358336306', 'e99a18c428cb38d5f260853678922e03', '', '', '', '', '', '', '', '', '', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-07-03 17:17:57', 0),
(6, 'Ashwin', 'Kumar', '9840802580', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '', '', '', '', '2', 'default-image.jpg', 'default-image.jpg', '2020-07-15 06:03:08', 0),
(7, 'Anjana', 'A', '9080319238', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '', '', '', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-08-17 08:28:29', 0),
(8, 'Shwetha', 'Sukumar', '9791783512', 'e10adc3949ba59abbe56e057f20f883e', '', 'Badminton', 'Raj\'s Academy', '1996-07-13', 'male', 'virugambakkam', 'chennai', 'tamilnadu', '600092', '', '4', 'spectra5f511d493c020.jpg', 'spectra5f511e04071cb.jpg', '2020-09-03 16:38:17', 0),
(9, 'Kumar', 'Raj', '5436354', 'e10adc3949ba59abbe56e057f20f883e', 'State Level National Player', 'Archery', '', '1996-07-13', 'male', 'Chenni', 'Chennai', 'Tamilnadu', '600092', '', '1', 'spectra5f5639c38ae06.jpg', 'spectra5f5639d01aabe.jpg', '2020-09-07 13:43:38', 0),
(10, 'Vignesh', 'Murali', '9600166393', 'd00f5d5217896fb7fd601412cb890830', '', '', '', '', '', '', '', '', '', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-09-07 13:51:11', 0),
(11, 'Vignesh', 'Bala', '09790849863', 'd00f5d5217896fb7fd601412cb890830', 'football', 'Football', '', '1997-08-07', 'male', 'near mit', 'chennai', 'Tamil Nadu', '600044', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-09-07 15:23:05', 0),
(12, 'Venkataraman', 'S', '7299007901', 'e10adc3949ba59abbe56e057f20f883e', 'hi', 'Archery', '', '1959-07-07', 'male', 'tiu', 'chennai', 'tn', '600044', '', '2', 'default-image.jpg', 'default-image.jpg', '2020-09-24 12:06:25', 0),
(13, 'Vignesh', 'Murali', '+919600166393', '5960fd887c8abde5882011b7cf735b3a', '', '', '', '', '', '', '', '', '', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-09-25 10:11:30', 0),
(14, 'Vijay', 'S', '9790896904', 'e10adc3949ba59abbe56e057f20f883e', 'sakdnk', 'Archery', '', '2020-11-19', 'male', 'asdsad', 'das', 'tn', '600044', '', '1', 'default-image.jpg', 'default-image.jpg', '2020-11-02 13:22:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int(15) NOT NULL,
  `category` int(15) NOT NULL DEFAULT 7,
  `user_id` int(15) NOT NULL,
  `image` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-image.jpg',
  `title` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect_link` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(12000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `category`, `user_id`, `image`, `title`, `short_description`, `redirect_link`, `description`, `date`, `date_time`, `status`) VALUES
(1, 7, 1, 'default-image.jpg', 'NASCAR investigating noose found in Bubba Wallace garage at Alabama race', ' NASCAR has launched an investigation after a noose was found in the garage stall of Bubba Wallace. (AP Photo)', '', 'TALLADEGA: NASCAR has launched an investigation after a noose was found in the garage stall of Bubba Wallace, the only Black driver in the elite Cup Series who just two weeks ago successfully pushed the stock car series to ban the Confederate flag at its venues.\r\n\r\nNASCAR said the noose was found on Sunday afternoon and vowed to do everything possible to find who was responsible and “eliminate them from the sport.”\r\n\r\n \r\n“We are angry and outraged, and cannot state strongly enough how seriously we take this heinous act,” the series said in a statement. “As we have stated unequivocally, there is no place for racism in NASCAR, and this act only strengthens our resolve to make the sport open and welcoming to all.”\r\n\r\nOn Twitter, Wallace said the “the despicable act of racism and hatred leaves me incredibly saddened and serves as a painful reminder of how much further we have to go as a society and how persistent we must be in the fight against racism.”\r\n\r\n“As my mother told me today, ‘They are just trying to scare you,’” he wrote. “ This will not break me, I will not give in nor will I back down. I will continue to proudly stand for what I believe in.”\r\n\r\nThe noose was discovered on the same day NASCAR’s fledgling flag ban faced its biggest challenge. The ban took effect before last week’s race near Miami, but there were only about 1,000 military members admitted into that race.\r\n\r\nAt Talladega, in the heart of the South, as many as 5,000 fans were allowed in, even though rain postponed the race until Monday and visitors were barred from the infield. No flags were spotted Sunday, but cars and pickup trucks driving along nearby roads were flying the flag and parading past the entrance to the superspeedway over the weekend. A small plane flew over the track Sunday pulling a banner with the flag and the words “Defund NASCAR.”\r\n\r\nWallace’s 2013 victory in a Truck Series race was only the second in a NASCAR national series by an Black driver (Wendell Scott, 1963) and helped push him into the Cup Series, where he drives the No. 43 for Hall of Famer Richard Petty and is forced to scramble for sponsorship dollars.\r\n\r\nWallace, a 26-year-old Alabama native, said he has found support among fellow drivers for his stance on the flag. He noted that in his tweet after the noose announcement.', '2020-07-07', '2020-07-07 17:54:16', 1),
(2, 2, 0, 'default-image.jpg', 'Dubai', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-08', '2020-07-08 11:20:16', 1),
(3, 4, 0, 'default-image.jpg', 'Terms and Conditions', '(Renewed) Cosmic Byte C3070W Nebula 2.4G Wireless Gamepad', '', '(Renewed) Cosmic Byte C3070W Nebula 2.4G Wireless Gamepad', '2020-07-08', '2020-07-08 11:21:42', 1),
(4, 1, 0, 'default-image.jpg', 'Terms and Conditions', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:43:51', 1),
(5, 3, 0, 'default-image.jpg', 'Iruttu Kadai Halwa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:45:09', 1),
(6, 2, 0, 'default-image.jpg', 'Dubai', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:46:45', 1),
(7, 3, 0, 'default-image.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:47:07', 1),
(8, 3, 0, 'default-image.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:48:19', 1),
(9, 3, 0, 'default-image.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:49:16', 1),
(10, 3, 0, 'default-image.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:50:17', 1),
(11, 3, 0, '2020090715942702715f06a23f8a5ed.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:51:11', 1),
(12, 7, 0, '2020090715942703495f06a28dbbb15.png', 'Dubai', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 04:52:29', 0),
(13, 7, 0, 'default-image.jpg', 'Dubai', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 05:35:27', 1),
(14, 7, 0, 'default-image.jpg', 'Dubai', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel velit volutpat, finibus metus eu, aliquet lorem. Phasellus sed mollis arcu, nec tincidunt diam. In auctor, lorem in dictum pretium, diam tortor cursus massa, vel tincidunt lacus augue vulputate purus. In tincidunt lobortis lectus a congue. Proin odio justo, hendrerit sed felis non, imperdiet egestas nibh. Morbi viverra pellentesque urna. Donec sagittis, risus eu pulvinar condimentum, enim orci volutpat leo, a consectetur lacus mauris ut erat. Proin dapibus ullamcorper tellus, eu eleifend velit aliquet id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', '2020-07-09', '2020-07-09 05:35:36', 1),
(15, 2, 0, '2020150715947916045f0e96b488775.jpg', 'Jason Holder rises to second spot in latest ICC Test rankings', 'Jason Holder moved up to second spot in the ICC Test Player Rankings after West Indies\' win at Southampton. Shannon Gabriel also moved up one spot into 18th place after a man of the match performance by picking nine wickets.', 'https://www.google.com/', 'https://www.sportskeeda.com/cricket/jason-holder-rises-second-spot-latest-icc-test-rankings?ref=cricketpage', '2020-07-14', '2020-07-15 05:40:04', 0),
(16, 1, 0, '2020310815988914765f4d25d444cd2.jpg', 'Matt Cardona was wrestling out of contract during his final year in WWE', 'Not really to be honest. When we found out the cuts will be coming that day, like I said, I’m always ready. It’s not just a cute hashtag. I already set up my Pro Wrestling Tees store. I’m like, I’m ready. Let’s go. Let’s go baby. It sounds crazy to say, but I wasn’t calling anyone begging for it, but inside I was begging for it please. Please let this happen because for over a year, I hadn’t signed a new contract.', 'https://www.sportskeeda.com/wwe/news-matt-cardona-wrestling-contract-final-year-wwe?ref=homepage', '', '2020-08-31', '2020-08-31 16:31:16', 0),
(17, 1, 0, '2020310815988915035f4d25efadbcb.jpg', 'Matt Hardy says Vince McMahon was ready to move him to backstage WWE role', 'I could see that Vince and WWE they already had it in their minds. In Vince\'s mind, he was ready to move me on from being a talent into working backstage. And you know, being a producer, helping give back and teach other guys behind the scenes. And I\'m very happy to do that...but a little later on. These last few years that I have left that I have to do this physically; I want to do it to the highest level that I can. I want to enjoy it because the whole reason I got into pro wrestling in the first place was because I love the idea of performing and being a pro wrestler. So it\'s like I don\'t want to give that up right now.', 'https://www.sportskeeda.com/wwe/news-matt-hardy-says-vince-mcmahon-ready-move-backstage-wwe-role?ref=homepage', '', '2020-08-31', '2020-08-31 16:31:43', 0),
(18, 1, 0, '2020310815988916345f4d267243251.jpg', 'List of teams that have come back from a 3-1 deficit to win a playoff series in NBA history', 'LeBron James and the Cleveland Cavaliers scripted the most astonishing comeback in NBA history when they overcame a 3-1 deficit in the 2016 NBA Finals to bring a championship to Cleveland for the first time in the history of the franchise.', 'https://www.sportskeeda.com/basketball/list-nba-teams-come-back-3-1-deficit-win-playoff-series?ref=homepage', '', '2020-08-31', '2020-08-31 16:33:54', 0),
(19, 1, 0, '2020310815988916675f4d2693727c3.jpg', 'WWE removes Brock Lesnar page and merchandise from online store', 'It is not known why Brock Lesnar’s merchandise is no longer available to buy, but it has not gone unnoticed by WWE fans on social media.', 'https://www.sportskeeda.com/wwe/news-wwe-removes-brock-lesnar-page-merchandise-online-store?ref=homepage', '', '2020-08-31', '2020-08-31 16:34:27', 0),
(20, 1, 0, '2020310815988940535f4d2fe5dc3b0.jpg', 'WWE RAW after Payback Preview: Seth Rollins gets a much-awaited match, Injured Superstar to return? (Aug 31, 2020)', 'Following a decent Payback, we are now set for the first episode of WWE RAW after the pay-per-view. There were two title matches involving the RAW Superstars at the PPV.', 'https://www.sportskeeda.com/wwe/wwe-raw-payback-preview-seth-rollins-gets-much-awaited-match-injured-superstar-return-aug-31-2020?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:13', 0),
(21, 2, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(22, 2, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(23, 2, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0),
(24, 3, 0, '2020150715947916045f0e96b488775.jpg', 'Jason Holder rises to second spot in latest ICC Test rankings', 'Jason Holder moved up to second spot in the ICC Test Player Rankings after West Indies\' win at Southampton. Shannon Gabriel also moved up one spot into 18th place after a man of the match performance by picking nine wickets.', 'https://www.google.com/', 'https://www.sportskeeda.com/cricket/jason-holder-rises-second-spot-latest-icc-test-rankings?ref=cricketpage', '2020-07-14', '2020-07-15 05:40:04', 0),
(25, 3, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(26, 3, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(27, 3, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0),
(28, 4, 0, '2020150715947916045f0e96b488775.jpg', 'Jason Holder rises to second spot in latest ICC Test rankings', 'Jason Holder moved up to second spot in the ICC Test Player Rankings after West Indies\' win at Southampton. Shannon Gabriel also moved up one spot into 18th place after a man of the match performance by picking nine wickets.', 'https://www.google.com/', 'https://www.sportskeeda.com/cricket/jason-holder-rises-second-spot-latest-icc-test-rankings?ref=cricketpage', '2020-07-14', '2020-07-15 05:40:04', 0),
(29, 4, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(30, 4, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(31, 4, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0),
(32, 5, 0, '2020150715947916045f0e96b488775.jpg', 'Jason Holder rises to second spot in latest ICC Test rankings', 'Jason Holder moved up to second spot in the ICC Test Player Rankings after West Indies\' win at Southampton. Shannon Gabriel also moved up one spot into 18th place after a man of the match performance by picking nine wickets.', 'https://www.google.com/', 'https://www.sportskeeda.com/cricket/jason-holder-rises-second-spot-latest-icc-test-rankings?ref=cricketpage', '2020-07-14', '2020-07-15 05:40:04', 0),
(33, 5, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(34, 5, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(35, 5, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0),
(36, 6, 0, '2020150715947916045f0e96b488775.jpg', 'Jason Holder rises to second spot in latest ICC Test rankings', 'Jason Holder moved up to second spot in the ICC Test Player Rankings after West Indies\' win at Southampton. Shannon Gabriel also moved up one spot into 18th place after a man of the match performance by picking nine wickets.', 'https://www.google.com/', 'https://www.sportskeeda.com/cricket/jason-holder-rises-second-spot-latest-icc-test-rankings?ref=cricketpage', '2020-07-14', '2020-07-15 05:40:04', 0),
(37, 6, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(38, 6, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(39, 6, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0),
(40, 7, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(41, 7, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(42, 7, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0),
(43, 8, 0, '2020150715947916045f0e96b488775.jpg', 'Jason Holder rises to second spot in latest ICC Test rankings', 'Jason Holder moved up to second spot in the ICC Test Player Rankings after West Indies\' win at Southampton. Shannon Gabriel also moved up one spot into 18th place after a man of the match performance by picking nine wickets.', 'https://www.google.com/', 'https://www.sportskeeda.com/cricket/jason-holder-rises-second-spot-latest-icc-test-rankings?ref=cricketpage', '2020-07-14', '2020-07-15 05:40:04', 0),
(44, 8, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(45, 8, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(46, 8, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0),
(47, 9, 0, '2020150715947916045f0e96b488775.jpg', 'Jason Holder rises to second spot in latest ICC Test rankings', 'Jason Holder moved up to second spot in the ICC Test Player Rankings after West Indies\' win at Southampton. Shannon Gabriel also moved up one spot into 18th place after a man of the match performance by picking nine wickets.', 'https://www.google.com/', 'https://www.sportskeeda.com/cricket/jason-holder-rises-second-spot-latest-icc-test-rankings?ref=cricketpage', '2020-07-14', '2020-07-15 05:40:04', 0),
(48, 9, 0, '2020310815988940855f4d3005c20e6.jpg', 'No going back on Messi exit, says Barca presidential candidate', 'Lionel Messi\'s exit from Barcelona is &quot;irrevocable&quot; according to prospective Camp Nou presidential candidate Toni Freixa, who expects the superstar forward to join Manchester City.', 'https://www.sportskeeda.com/football/no-going-back-on-messi-exit-says-barca-presidential-candidate?ref=homepage', '', '2020-08-31', '2020-08-31 17:14:45', 0),
(49, 9, 0, '2020310815988941385f4d303ad4323.jpg', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'Could WWE elevate Aleister Black in a possible feud with Drew McIntyre?', 'https://www.sportskeeda.com/wwe/rumor-could-wwe-elevate-aleister-black-possible-feud-drew-mcintyre?ref=homepage', '', '2020-08-31', '2020-08-31 17:15:38', 0),
(50, 9, 0, '2020310815988943005f4d30dc23dfe.jpg', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'Reports: Real Madrid star James Rodriguez set for medical with Premier League side', 'https://www.sportskeeda.com/football/rumor-reports-real-madrid-star-james-rodriguez-set-medical-premier-league-side?ref=homepage', '', '2020-08-31', '2020-08-31 17:18:20', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_category`
--

CREATE TABLE `tbl_news_category` (
  `id` int(15) NOT NULL,
  `name` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_news_category`
--

INSERT INTO `tbl_news_category` (`id`, `name`, `date_time`, `status`) VALUES
(1, 'Highlights / Trending Now', '2020-07-07 15:51:10', 0),
(2, 'International', '2020-07-07 15:51:48', 0),
(3, 'Domestic', '2020-07-07 15:51:48', 0),
(4, 'University', '2020-07-07 15:51:48', 0),
(5, 'Organization', '2020-07-07 15:51:48', 0),
(6, 'Corporate / Company', '2020-07-07 15:51:48', 0),
(7, 'Others', '2020-07-07 15:51:48', 0),
(8, 'Popular Post', '2020-07-07 15:51:48', 0),
(9, 'Latest news', '2020-07-07 15:51:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_likes`
--

CREATE TABLE `tbl_news_likes` (
  `id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `news_id` int(15) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_password`
--

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` bigint(20) NOT NULL DEFAULT 1,
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(2, 'System Administrator'),
(3, 'Manager'),
(4, 'Employee'),
(1, 'Super Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_services`
--

CREATE TABLE `tbl_services` (
  `id` int(15) NOT NULL,
  `name` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(12000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_services`
--

INSERT INTO `tbl_services` (`id`, `name`, `image`, `short_description`, `description`, `date_time`, `status`) VALUES
(1, 'Individual', '', '', '', '2020-05-27 14:45:31', 0),
(2, 'Coach', '', '', '', '2020-05-27 14:45:44', 0),
(3, 'Mentor', '', '', '', '2020-05-27 14:46:10', 0),
(4, 'Academy', '', '', '', '2020-06-09 05:21:31', 0),
(5, 'University', '', '', '', '2020-06-09 17:43:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_request`
--

CREATE TABLE `tbl_service_request` (
  `id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `service` int(15) NOT NULL,
  `name` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sport` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name_of_facility` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `level_of_participation` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purpose` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `raised_amount` int(15) NOT NULL DEFAULT 0,
  `fund_required` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `raiser_image` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-image.jpg',
  `described_purpose` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `career_option` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `specific_requirements` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `proof_upload` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `request_status` varchar(900) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_service_request`
--

INSERT INTO `tbl_service_request` (`id`, `user_id`, `service`, `name`, `date_of_birth`, `gender`, `sport`, `address`, `city`, `state`, `pincode`, `designation`, `name_of_facility`, `level_of_participation`, `purpose`, `raised_amount`, `fund_required`, `raiser_image`, `described_purpose`, `career_option`, `specific_requirements`, `proof_upload`, `request_status`, `date_time`, `status`) VALUES
(1, 2, 1, 'praveen murali', '2020-07-06', 'male', '1', 'virugambakkam, qq, qq', 'chennai', 'Tamil Nadu', '600092', 'fgh', 'dskjlgh', 'dlkfjsh', '', 0, '', 'default-image.jpg', '', '', 'sdlkfj', '2020060715940276155f02ee5f5c772.png', 'pending', '2020-07-06 09:26:55', 0),
(2, 2, 1, 'praveenn', '2020-07-06', 'male', '1', 'virugambakkam', 'chennai', 'tamilnadu', '600092', 'sdf', 'sdf', 'sdf', '', 0, '', 'default-image.jpg', '', '', 'sdf', '2020060715940276775f02ee9d745db.png', 'pending', '2020-07-06 09:27:57', 0),
(3, 2, 4, 'Praveen Murali', '1993-06-18', 'male', '6', 'No. 77 subraniam, rail nagar road, kadaperi, west tambaram', 'Chennai', 'Tamilnadu', '600045', 'CEO', 'Praveen', '', 'My Sister Amrita Is In Urgent Need Of Kidney And Liver Transplant. I Need Your Support', 0, '200000', '2020040915992208365f522c642cba3.jpg', '&lt;p&gt;My sister, Amrita Datta, is suffering from Chronic polycystic kidney and liver disease and needs to undergo a multiple organ transplant at the earliest to survive. To tell you a little about Amrita, she’s a young, vivacious and independent girl. While she’s a senior HR professional by day, she’s an artist by heart. It’s her dream to cross over to the creative side and be a full-time artist someday. Amrita’s condition is genetic and she has already undergone two surgeries. The first surgery was in June 2017- a laparoscopic fenestration to burst some of the bigger cysts. But they recurred within two years more aggressively so the second surgery was performed in December 2019. This was an open abdominal surgery with an 18 inches incision. While we were hopeful of a partial recovery, it only started getting worse. Her kidneys and liver have enlarged abnormally occupying most of the abdominal region. As a result, her lungs have partially collapsed, making it incredibly hard for her to breathe or even perform normal activities. The organs take up so much space that her stomach has shrunk making it hard for her to eat, resulting in severe malnutrition. Since March 2020 she has been undergoing treatment at Dr. Rela Institute &amp;amp; Medical Centre and was due to have her transplant surgery in April 2020. She’s already registered on the organ recipient list but due to the COVID19 imposed travel restrictions, the entire process has now been de&lt;/p&gt;', '', 'Please help us raise this amount by clicking on the donate button and sharing this page with your friends and family.', '', 'approved', '2020-08-09 08:09:27', 0),
(4, 2, 4, 'Praveen Murali', '1993-06-18', 'male', '6', 'No. 77 subraniam, rail nagar road, kadaperi, west tambaram', 'Chennai', 'Tamilnadu', '600045', 'Owner', 'Harishkalyan', '', 'My Sister Amrita Is In Urgent Need Of Kidney And Liver Transplant. I Need Your Support', 0, '200000', '2020040915992204075f522ab70e114.jpg', '&lt;p&gt;My sister, Amrita Datta, is suffering from Chronic polycystic kidney and liver disease and needs to undergo a multiple organ transplant at the earliest to survive. To tell you a little about Amrita, she’s a young, vivacious and independent girl. While she’s a senior HR professional by day, she’s an artist by heart. It’s her dream to cross over to the creative side and be a full-time artist someday. Amrita’s condition is genetic and she has already undergone two surgeries. The first surgery was in June 2017- a laparoscopic fenestration to burst some of the bigger cysts. But they recurred within two years more aggressively so the second surgery was performed in December 2019. This was an open abdominal surgery with an 18 inches incision. While we were hopeful of a partial recovery, it only started getting worse. Her kidneys and liver have enlarged abnormally occupying most of the abdominal region. As a result, her lungs have partially collapsed, making it incredibly hard for her to breathe or even perform normal activities. The organs take up so much space that her stomach has shrunk making it hard for her to eat, resulting in severe malnutrition. Since March 2020 she has been undergoing treatment at Dr. Rela Institute &amp;amp; Medical Centre and was due to have her transplant surgery in April 2020. She’s already registered on the organ recipient list but due to the COVID19 imposed travel restrictions, the entire process has now been de&lt;/p&gt;', '', 'Please help us raise this amount by clicking on the donate button and sharing this page with your friends and family.', '2020090815969607925f2fb018d0150.jpeg', 'approved', '2020-08-09 08:13:12', 0),
(5, 2, 4, 'Praveen Murali', '1993-06-18', 'male', '6', 'No. 77 subraniam, rail nagar road, kadaperi, west tambaram', 'Chennai', 'Tamilnadu', '600045', 'CEO', 'Rishi', '', 'My Sister Amrita Is In Urgent Need Of Kidney And Liver Transplant. I Need Your Support', 27, '200000', '2020040915991982555f51d42fed1f3.jpg', '&lt;p&gt;My sister, Amrita Datta, is suffering from Chronic polycystic kidney and liver disease and needs to undergo a multiple organ transplant at the earliest to survive. To tell you a little about Amrita, she’s a young, vivacious and independent girl. While she’s a senior HR professional by day, she’s an artist by heart. It’s her dream to cross over to the creative side and be a full-time artist someday. Amrita’s condition is genetic and she has already undergone two surgeries. The first surgery was in June 2017- a laparoscopic fenestration to burst some of the bigger cysts. But they recurred within two years more aggressively so the second surgery was performed in December 2019. This was an open abdominal surgery with an 18 inches incision. While we were hopeful of a partial recovery, it only started getting worse. Her kidneys and liver have enlarged abnormally occupying most of the abdominal region. As a result, her lungs have partially collapsed, making it incredibly hard for her to breathe or even perform normal activities. The organs take up so much space that her stomach has shrunk making it hard for her to eat, resulting in severe malnutrition. Since March 2020 she has been undergoing treatment at Dr. Rela Institute &amp;amp; Medical Centre and was due to have her transplant surgery in April 2020. She’s already registered on the organ recipient list but due to the COVID19 imposed travel restrictions, the entire process has now been de1111111111111111111111111111111111&lt;/p&gt;', '', 'Please help us raise this amount by clicking on the donate button and sharing this page with your friends and family.', '2020090815969607925f2fb018d0150.jpeg', 'approved', '2020-08-09 08:13:12', 0),
(6, 12, 1, 'Venkataraman S', '1959-07-07', 'male', '1', 'tiu', 'chennai', 'tn', '600044', 'cricketer', 'Sai Ram ', 'state', '', 0, '', 'default-image.jpg', '', '', 'Looking for coach ', '', 'pending', '2020-09-24 12:17:57', 0),
(7, 12, 1, 'Venkataraman S', '1959-07-07', 'male', '1', 'tiu', 'chennai', 'tn', '600044', 'cricketer', 'Sai Ram ', 'state', '', 0, '', 'default-image.jpg', '', '', 'Looking for coach ', '', 'pending', '2020-09-24 12:17:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sports`
--

CREATE TABLE `tbl_sports` (
  `id` int(15) NOT NULL,
  `name` varchar(3500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-image.jpg',
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_sports`
--

INSERT INTO `tbl_sports` (`id`, `name`, `image`, `status`) VALUES
(1, 'Archery', 'default-image.jpg', 0),
(2, 'Athletics', 'default-image.jpg', 0),
(3, 'Badminton', 'default-image.jpg', 0),
(4, 'Baseball', 'default-image.jpg', 0),
(5, 'Basketball', 'default-image.jpg', 0),
(6, 'Billiards', 'default-image.jpg', 0),
(7, 'Bodybuilding', 'default-image.jpg', 0),
(8, 'Boxing', 'default-image.jpg', 0),
(9, 'Carrom', 'default-image.jpg', 0),
(10, 'Chess', 'default-image.jpg', 0),
(11, 'Climbing', 'default-image.jpg', 0),
(12, 'Cricket', 'default-image.jpg', 0),
(13, 'Cycling', 'default-image.jpg', 0),
(14, 'Fencing', 'default-image.jpg', 0),
(15, 'Field hockey', 'default-image.jpg', 0),
(16, 'Football', 'default-image.jpg', 0),
(17, 'Golf', 'default-image.jpg', 0),
(18, 'Gymnastics', 'default-image.jpg', 0),
(19, 'Handball', 'default-image.jpg', 0),
(20, 'Horse riding', 'default-image.jpg', 0),
(21, 'Judo', 'default-image.jpg', 0),
(22, 'Kabaddi', 'default-image.jpg', 0),
(23, 'Kalari', 'default-image.jpg', 0),
(24, 'Karate', 'default-image.jpg', 0),
(25, 'Kho-Kho', 'default-image.jpg', 0),
(26, 'Kickboxing', 'default-image.jpg', 0),
(27, 'Kung fu', 'default-image.jpg', 0),
(28, 'Marathon', 'default-image.jpg', 0),
(29, 'MMA', 'default-image.jpg', 0),
(30, 'Motor Sports', 'default-image.jpg', 0),
(31, 'Powerlifting', 'default-image.jpg', 0),
(32, 'Rowing', 'default-image.jpg', 0),
(33, 'Rugby', 'default-image.jpg', 0),
(34, 'Sailing', 'default-image.jpg', 0),
(35, 'Scuba diving', 'default-image.jpg', 0),
(36, 'Shooting', 'default-image.jpg', 0),
(37, 'Skating', 'default-image.jpg', 0),
(38, 'Snooker', 'default-image.jpg', 0),
(39, 'Squash', 'default-image.jpg', 0),
(40, 'Surfing', 'default-image.jpg', 0),
(41, 'Swimming', 'default-image.jpg', 0),
(42, 'Table Tennis', 'default-image.jpg', 0),
(43, 'Taekwondo', 'default-image.jpg', 0),
(44, 'Tennis', 'default-image.jpg', 0),
(45, 'Throwball', 'default-image.jpg', 0),
(46, 'Triathlon', 'default-image.jpg', 0),
(47, 'Tug of war', 'default-image.jpg', 0),
(48, 'Volleyball', 'default-image.jpg', 0),
(49, 'Walking', 'default-image.jpg', 0),
(50, 'Weightlifting', 'default-image.jpg', 0),
(51, 'Wrestling', 'default-image.jpg', 0),
(52, 'Wushu', 'default-image.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'admin@spectra.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'System Administrator', '6374100133', 2, 0, 0, '2015-07-01 18:56:49', 1, '2017-06-19 09:22:53'),
(2, 'manager@spectra.com', '$2y$10$Gkl9ILEdGNoTIV9w/xpf3.mSKs0LB1jkvvPKK7K0PSYDsQY7GE9JK', 'Manager', '6374100133', 3, 0, 1, '2016-12-09 17:49:56', 1, '2017-06-19 09:22:29'),
(3, 'employee@spectra.com', '$2y$10$MB5NIu8i28XtMCnuExyFB.Ao1OXSteNpCiZSiaMSRPQx1F1WLRId2', 'Employee', '6374100133', 4, 0, 1, '2016-12-09 17:50:22', 1, '2017-06-19 09:23:21'),
(4, 'sadmin@spectra.com', '$2y$10$SAvFim22ptA9gHVORtIaru1dn9rhgerJlJCPxRNA02MjQaJnkxawq', 'Super Admin', '6374100133', 1, 0, 1, '2020-02-20 07:12:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_acheivements`
--

CREATE TABLE `tbl_user_acheivements` (
  `id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `title` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `issued_by` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proof_image` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-image.jpg',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_acheivements`
--

INSERT INTO `tbl_user_acheivements` (`id`, `user_id`, `title`, `date`, `place`, `position`, `issued_by`, `proof_image`, `date_time`, `status`) VALUES
(1, 2, 'Running Race 100 mtrs', '2020-08-03', 'chennai', 'First place', 'MZCET', 'spectra5f2873baa1397.jpg', '2020-08-03 20:29:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_gallery`
--

CREATE TABLE `tbl_user_gallery` (
  `id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `image` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-image.jpg',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_gallery`
--

INSERT INTO `tbl_user_gallery` (`id`, `user_id`, `image`, `date_time`, `status`) VALUES
(1, 2, 'default-image.jpg', '2020-08-03 21:05:14', 0),
(2, 2, 'default-image.jpg', '2020-08-03 21:05:55', 0),
(3, 2, 'spectra5f287c4fc81c6.jpg', '2020-08-03 21:06:23', 0),
(4, 2, 'spectra5f287c586df80.jpg', '2020-08-03 21:06:32', 0),
(5, 2, 'spectra5f2950c4c459a.jpg', '2020-08-04 12:12:52', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_all_services`
--
ALTER TABLE `tbl_all_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cf_category`
--
ALTER TABLE `tbl_cf_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_donors`
--
ALTER TABLE `tbl_donors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_events`
--
ALTER TABLE `tbl_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_friend_request`
--
ALTER TABLE `tbl_friend_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_general_settings`
--
ALTER TABLE `tbl_general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_general_users`
--
ALTER TABLE `tbl_general_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news_category`
--
ALTER TABLE `tbl_news_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news_likes`
--
ALTER TABLE `tbl_news_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `tbl_services`
--
ALTER TABLE `tbl_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_service_request`
--
ALTER TABLE `tbl_service_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sports`
--
ALTER TABLE `tbl_sports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `tbl_user_acheivements`
--
ALTER TABLE `tbl_user_acheivements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_gallery`
--
ALTER TABLE `tbl_user_gallery`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_all_services`
--
ALTER TABLE `tbl_all_services`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_cf_category`
--
ALTER TABLE `tbl_cf_category`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_donors`
--
ALTER TABLE `tbl_donors`
  MODIFY `id` int(155) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_events`
--
ALTER TABLE `tbl_events`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_friend_request`
--
ALTER TABLE `tbl_friend_request`
  MODIFY `id` int(155) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_general_settings`
--
ALTER TABLE `tbl_general_settings`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_general_users`
--
ALTER TABLE `tbl_general_users`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `tbl_news_category`
--
ALTER TABLE `tbl_news_category`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_news_likes`
--
ALTER TABLE `tbl_news_likes`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_services`
--
ALTER TABLE `tbl_services`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_service_request`
--
ALTER TABLE `tbl_service_request`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_sports`
--
ALTER TABLE `tbl_sports`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_user_acheivements`
--
ALTER TABLE `tbl_user_acheivements`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user_gallery`
--
ALTER TABLE `tbl_user_gallery`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
