!(function($) {
  "use strict";
 
  // Smooth scroll for the navigation menu and links with .scrollto classes
  $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function(e) {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      e.preventDefault();
      var target = $(this.hash);
      if (target.length) {

        var scrollto = target.offset().top;
        var scrolled = 0;

        if ($('#header').length) {
          scrollto -= $('#header').outerHeight()

          if (!$('#header').hasClass('header-scrolled')) {
            scrollto += scrolled;
          }
        }

        if ($(this).attr("href") == '#header') {
          scrollto = 0;
        }

        $('html, body').animate({
          scrollTop: scrollto
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.nav-menu, .mobile-nav').length) {
          $('.nav-menu .active, .mobile-nav .active').removeClass('active');
          $(this).closest('li').addClass('active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Mobile Navigation
  if ($('.nav-menu').length) {
    var $mobile_nav = $('.nav-menu').clone().prop({
      class: 'mobile-nav d-lg-none'
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
    $('body').append('<div class="mobile-nav-overly"></div>');

    $(document).on('click', '.mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
      $('.mobile-nav-overly').toggle();
    });

    $(document).on('click', '.mobile-nav .drop-down > a', function(e) {
      e.preventDefault();
      $(this).next().slideToggle(300);
      $(this).parent().toggleClass('active');
    });

    $(document).click(function(e) {
      var container = $(".mobile-nav, .mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
      }
    });
  } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
  }

  // Toggle .header-scrolled class to #header when page is scrolled
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
	  $('#sidebar').addClass('is-sticky-sidebar');
    } else {
      $('#header').removeClass('header-scrolled');
	  $('#sidebar').removeClass('is-sticky-sidebar');
    }
  });

  // Stick the header at top on scroll
  $("#header").sticky({
    topSpacing: 0,
    zIndex: '50'
  });

  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });

  $('.back-to-top').click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 1500, 'easeInOutExpo');
    return false;
  });
  // Initiate the venobox plugin
  $(window).on('load', function() {
    $('.venobox').venobox();
  });

  // Testimonial carousel (uses the Owl Carousel library)
  $(".testimonial-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: false,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      900: {
        items: 2
      }
    }
  });
	$(".support-them-carousel").owlCarousel({
		autoplay: true,
		dots: false,
		loop: false,
		nav: false,
		responsive: {
		  0: {
			items: 1
		  },
		  768: {
			items: 2
		  },
		  900: {
			items: 3
		  }
		}
	});

	$(".home-idols-carousel").owlCarousel({
		autoplay: true,
		dots: false,
		loop: false,
		nav: false,
		responsive: {
		  0: {
			items: 1
		  },
		  768: {
			items: 2
		  },
		  900: {
			items: 3
		  }
		}
	});

  // Events carousel (uses the Owl Carousel library)
  $(".events-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: false,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      900: {
        items: 3
      }
    }
  });

  // Clients carousel (uses the Owl Carousel library)
  $(".clients-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: false,
    responsive: {
      0: {
        items: 2
      },
      768: {
        items: 4
      },
      900: {
        items: 8
      }
    }
  });

  // Testimonials carousel (uses the Owl Carousel library)
  $(".testimonials-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: false,
    items: 1
  });

  // Porfolio isotope and filter
  $(window).on('load', function() {
    var portfolioIsotope = $('.portfolio-container').isotope({
      itemSelector: '.portfolio-item',
      layoutMode: 'fitRows'
    });

    $('#portfolio-flters li').on('click', function() {
      $("#portfolio-flters li").removeClass('filter-active');
      $(this).addClass('filter-active');

      portfolioIsotope.isotope({
        filter: $(this).data('filter')
      });
    });

    // Initiate venobox (lightbox feature used in portofilo)
    $(document).ready(function() {
      $('.venobox').venobox();
    });
  });

  // Initi AOS
  AOS.init({
    duration: 1000,
    easing: "ease-in-out-back"
  });

})(jQuery);


$.fn.jQuerySimpleCounter = function( options ) {
	var settings = $.extend({
		start:  0,
		end:    100,
		easing: 'swing',
		duration: 400,
		complete: ''
	}, options );

	var thisElement = $(this);

	$({count: settings.start}).animate({count: settings.end}, {
		duration: settings.duration,
		easing: settings.easing,
		step: function() {
			var mathCount = Math.ceil(this.count);
			thisElement.text(mathCount);
		},
		complete: settings.complete
	});
};


	$('#number1').jQuerySimpleCounter({end: $('#number1').attr('data-number'),duration: 3000});
	$('#number2').jQuerySimpleCounter({end: $('#number2').attr('data-number'),duration: 3000});
	$('#number3').jQuerySimpleCounter({end: $('#number3').attr('data-number'),duration: 2000});
	$('#number4').jQuerySimpleCounter({end: $('#number4').attr('data-number'),duration: 2500});
	$('#number5').jQuerySimpleCounter({end: $('#number5').attr('data-number'),duration: 2500});

	$('.form_register').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "registerPost",
			data: $(this).serialize(),
			dataType: "json",
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					toastr.success('Thanks for registering with us!');
					$('button[type=submit]').removeAttr('disabled');
					setTimeout(function(){ window.location.assign(baseURL + "my-account"); }, 1000);
				}
				else if(response.result == 2)
				{
					toastr.error('Mobile number already registered with us!');
					$('button[type=submit]').removeAttr('disabled');
				}
				else
				{
					toastr.error('Something went wrong! Please try again later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.form_login').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "loginPost",
			data: $(this).serialize(),
			dataType: "json",
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					$('button[type=submit]').removeAttr('disabled');
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else if(response.result == 2)
				{
					toastr.error('Your account was blocked! Please contact support!');
					$('button[type=submit]').removeAttr('disabled');
				}
				else
				{
					toastr.error('Incorrect credentials!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.update-my-profile').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "update-my-profile",
			data: $(this).serialize(),
			dataType: "json",
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					toastr.success('Your profile has been updated!');
					$('button[type=submit]').removeAttr('disabled');
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.my-profile-change-password').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "my-profile-change-password",
			data: $(this).serialize(),
			dataType: "json",
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					$('.my-profile-change-password')[0].reset();
					toastr.success('Your profile has been updated!');
					$('button[type=submit]').removeAttr('disabled');
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.new-event').submit(function(e){
		e.preventDefault();
		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}
		$.ajax({
			type: 'POST',
			url: baseURL + "new-event",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					toastr.success('New event has been created!');
					$('button[type=submit]').removeAttr('disabled');
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.edit-event-post').submit(function(e){
		e.preventDefault();
		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}
		$.ajax({
			type: 'POST',
			url: baseURL + "edit-event-post",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					toastr.success('Event details has been updated!');
					$('button[type=submit]').removeAttr('disabled');
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.add-acheivements').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "add-acheivements",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					toastr.success('Your profile has been updated!');
					$('button[type=submit]').removeAttr('disabled');
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.form-donate-now').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "form-donate-now",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					payment(response.payment_url);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.user-gallery-images').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "user-gallery-images-upload",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					toastr.success('Your profile has been updated!');
					$('button[type=submit]').removeAttr('disabled');
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
				}
			}
		});
	});
	
	$('.profile-image-form').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "profile-image-form",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(response){
				if(response.result == 1)
				{
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
				}
			}
		});
	});
	
	$('.profile-banner-image-form').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: baseURL + "profile-banner-image-form",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(response){
				if(response.result == 1)
				{
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
				}
			}
		});
	});
	
	$('.service-new-request').submit(function(e){
        console.log("working on service request");
		e.preventDefault();		
		$.ajax({
			type: 'POST',
			url: baseURL + "service-new-request",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
				$('button[type=submit]').text('Loading...');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					if(response.payment_url!=null){
                        payment(response.payment_url);
                    }else{
                        toastr.success('Your Service Subscription is Success!');
					    $('button[type=submit]').removeAttr('disabled');
					    setTimeout(function(){ location.reload(); }, 2000);
                    }
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
					$('button[type=submit]').text('Submit');
				}
			}
		});
	});
	
	$('#delete-confirm form').submit(function(e){
		e.preventDefault();		
		if($('#delete-confirm form input[name=post-id]').val() !== "")
		{
			$.ajax({
				type: 'POST',
				url: baseURL + "post-delete",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('button[type=submit]').attr('disabled', 'true');
					$('button[type=submit]').text('Loading...');
				},
				success: function(response){
					console.log(response)
					if(response.result == 1)
					{
						toastr.success('Post has been deleted successfully!');
						$('button[type=submit]').removeAttr('disabled');
						setTimeout(function(){ location.reload(); }, 1000);
					}
					else
					{
						toastr.error('Something went wrong! Please try later!');
						$('button[type=submit]').removeAttr('disabled');
						$('button[type=submit]').text('Submit');
					}
				}
			});
		}
		else
		{
			toastr.error("Something went wrong! Please try later!");
		}
	});
	
	$('.member_follow_form').submit(function(e){
		e.preventDefault();		
		$.ajax({
			type: 'POST',
			url: baseURL + "member_follow_form",
			data: $(this).serialize(),
			dataType: "json",
			beforeSend: function() {
				$('button[type=submit]').attr('disabled', 'true');
				$('button[type=submit]').text('Loading...');
			},
			success: function(response){
				console.log(response)
				if(response.result == 1)
				{
					toastr.success('Follow request has been sent to member!');
					$('button[type=submit]').removeAttr('disabled');
					setTimeout(function(){ location.reload(); }, 1000);
				}
				else
				{
					toastr.error('Something went wrong! Please try later!');
					$('button[type=submit]').removeAttr('disabled');
					$('button[type=submit]').text('Submit');
				}
			}
		});
	});
	
	jQuery(function ($) {
		$(".sidebar-dropdown > a").click(function() {
			$(".sidebar-submenu").slideUp(200);
			if ($(this).parent().hasClass("active"))
			{
				$(".sidebar-dropdown").removeClass("active");
				$(this).parent().removeClass("active");
			} 
			else 
			{
				$(".sidebar-dropdown").removeClass("active");
				$(this).next(".sidebar-submenu").slideDown(200);
				$(this).parent().addClass("active");
			}
		});

		$("#close-sidebar").click(function() {
		  $(".page-wrapper").removeClass("toggled");
		});
		$("#show-sidebar").click(function() {
		  $(".page-wrapper").addClass("toggled");
		});
	   
	});
	
	$('.form_register select[name=service]').change(function(){
		if($(this).val() == "4" || $(this).val() == "5")
		{
			$(this).parent('.form-row').next().html('<p class="form-row form-row-wide">'+
									'<label for="organization_name">Oganization Name <span class="required">*</span></label>'+
									'<input type="text" class="form-control" name="organization_name" id="organization_name" required="">'+
								'</p>');
		}
		else
		{
			$(this).parent('.form-row').next().html('');
		}
	})


	

	jQuery(document).ready(function($){
		// Get current path and find target link
		var path = window.location.href;
		console.log(path,'path');
		// Account for home page with empty path
		if ( path == '' ) {
		  path = 'index.php';
		}
			
		var target = $('nav a[href="'+path+'"]');
		// Add active class to target link
		target.addClass('active');
	  });