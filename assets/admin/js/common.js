/**
 * @
 * uthor Praveen Murali
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});
	
});

	$('.insert_data').submit(function(e){
		e.preventDefault();
		
		var this_id = 'form[this_id=' + $(this).attr('this_id') + ']';
		
		$('#editor').val($('.ck-content').html()); 
		
		if(is_required(this_id) === true)
		{
			$.ajax({
				type: 'POST',
				url: baseURL + "admin/insert",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(this_id + ' input[type=submit]').attr('disabled', 'true');
				},
				success: function(response){
					console.log(response)
					if(response.result == 1)
					{
						$(this_id)[0].reset();
						toastr.success('Success!');
						$(this_id + ' input[type=submit]').removeAttr('disabled');
						
						if($(this_id).attr('row-data') === 'yes')
						{
							update_table_data(this_id);
						}
					}
					else
					{
						toastr.error('Something went wrong! Please try again later!');
						$(this_id + ' input[type=submit]').removeAttr('disabled');
					}
				}
			});
		}
		else
		{
			toastr.error('Please check the required fields!');
		}
	});


	$('.insert1_data').submit(function(e){
		e.preventDefault();
		
		var this_id = 'form[this_id=' + $(this).attr('this_id') + ']';
		
		$('#editor').val($('.ck-content').html()); 
		
		if(is_required(this_id) === true)
		{
			$.ajax({
				type: 'POST',
				url: baseURL + "admin/insert1",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(this_id + ' input[type=submit]').attr('disabled', 'true');
				},
				success: function(response){
					console.log(response)
					if(response.result == 1)
					{
						$(this_id)[0].reset();
						toastr.success('Success!');
						$(this_id + ' input[type=submit]').removeAttr('disabled');
						
						if($(this_id).attr('row-data') === 'yes')
						{
							update_table_data(this_id);
						}
					}
					else
					{
						toastr.error('Something went wrong! Please try again later!');
						$(this_id + ' input[type=submit]').removeAttr('disabled');
					}
				}
			});
		}
		else
		{
			toastr.error('Please check the required fields!');
		}
	});
	
	$('.update_data').submit(function(e){
		e.preventDefault();
		
		var this_id = 'form[this_id=' + $(this).attr('this_id') + ']';
		
		$('#editor').val($('.ck-content').html());
		
		if(is_required(this_id) === true)
		{
			$.ajax({
				type: 'POST',
				url: baseURL + "admin/update",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(this_id + ' input[type=submit]').attr('disabled', 'true');
				},
				success: function(response){
					if(response.result == 1)
					{
						toastr.success('Success!');
						$(this_id + ' input[type=submit]').removeAttr('disabled');
						
						if($(this_id).attr('reload-action') === 'true')
						{
							setTimeout(function(){ location.reload(); }, 1000);
						}
					}
					else
					{
						toastr.error('Something went wrong! Please try again later!');
						$(this_id + ' input[type=submit]').removeAttr('disabled');
					}
				}
			});
		}
		else
		{
			toastr.error('Please check the required fields!');
		}
	});
	
	function is_required(this_id){
		$('.error-span').hide();
		var inc = 0;
		$(this_id + " .required").each(function(){
			console.log($(this).attr('name'));
			if($(this).val() !== "undefined")
			{
				if($(this).val() != null)
				{
					if(($(this).val()).length > 0)
					{
						
					}
					else
					{
						console.log($(this).attr('name'));
						$(this).next("span").show();
						inc++;
					}
				}
				else
				{
					toastr.error('Something went wrong on ' + $(this).attr('name'));
					inc++;
				}
			}		
		});
		if(inc === 0)
		{
			return true;
		}
		return false;
	}
	