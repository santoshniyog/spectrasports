<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Frontend extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('frontend_model');
		
		$this->global['pageTitle'] = $this->config->item('app_name');
		
		$this->global['tis'] = $this;
		
		if(isset($_SESSION['is_login']))
		{
			$this->global['user_details'] = $this->frontend_model->get_records('tbl_general_users', "id = '" . $_SESSION['login_id'] . "'")[0];
			$this->global['login_name'] = ucfirst($this->global['user_details']->first_name) . " " . ucfirst($this->global['user_details']->last_name);
			if($this->global['user_details']->short_bio == "")
			{
				$this->session->set_flashdata('my-account-error-msg', 'yes');
				if($this->router->fetch_method() != "my_account" && $this->router->fetch_method() != "update_my_profile")
				{
					redirect("my-account");
				}
				
			}
		}
		$this->global['recent_fixtures'] = $this->get_fixtures();
    }
    
    public function index()
    {
        $this->global['pageTitle'] = $this->config->item('app_name');
        
        $this->loadPage("index", $this->global);
    }
	
    public function news()
    { 
		$this->global['pageTitle'] = "News" . $this->config->item('app_name');
		
        $this->global['news_page'] = 1;
        
        $this->loadPage("news", $this->global);
    }
	
    public function event($id)
    {
        $this->global['record'] = $this->frontend_model->get_records('tbl_events', "id = '$id' order by id desc")[0];
        
		$this->global['pageTitle'] = $this->global['record']->event_name . '- ' . $this->config->item('app_name'); 
		
        $this->loadPage("event", $this->global);
    }
	
    public function news_post($id, $slug)
    {
		$news_det = $this->frontend_model->get_records("tbl_news", "status = '0' and id = '$id'");
		if(sizeof($news_det) > 0)
		{
			$news_det = $news_det[0];
			$this->global['pageTitle'] = $news_det->title . " - " . $this->config->item('app_name');
			
			$this->global['news_page'] = 1;
			
			$this->global['news_detail'] = $news_det;
			
			$this->loadPage("news-detail", $this->global);
		}
		else
		{
			redirect("404");
		}
    }
	
    public function news_posts($id, $slug)
    {
		$news_cat = $this->frontend_model->get_records("tbl_news_category", "status = '0' and id = '$id'");
		if(sizeof($news_cat) > 0)
		{
			$news_cat = $news_cat[0];
			$this->global['pageTitle'] = $news_cat->name . " - " . $this->config->item('app_name');
			
			$this->global['news_page'] = 1;
			
			$this->global['news_category'] = $news_cat;
			$this->global['news_details'] = $this->frontend_model->get_records("tbl_news", "status = '0' and category = '$id'");
			
			$this->loadPage("news-category", $this->global);
		}
		else
		{
			redirect("404");
		}
    }
	
    public function my_account()
    {
		if(!isset($_SESSION['is_login']))
		{
			redirect('/');
		}
		
		$this->global['pageTitle'] = "Account Settings - " . $this->config->item('app_name');
		
		$this->loadPage("my-account", $this->global);
    }
	
    public function my_profile()
    {
		if(!isset($_SESSION['is_login']))
		{
			redirect('/');
		}
        $this->global['pageTitle'] = 'My Profile - ' . $this->config->item('app_name');
		
        $this->loadPage("my-profile", $this->global);
    }
	
    public function post_delete()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$post_id = $this->input->post('post-id');
			$info['status'] = 1;
			if($this->frontend_model->update('tbl_user_acheivements', $info, "id = " . $post_id))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function user_profile($id)
    {
		$user_det = $this->frontend_model->get_records("tbl_general_users", "id = '$id' and status = '0'");
		if(sizeof($user_det) > 0)
		{
			$user_det = $user_det[0];
			
			$this->global['record'] = $user_det;
			
			$this->global['pageTitle'] = ucfirst($user_det->first_name) . " " . ucfirst($user_det->last_name) . ' - ' . $this->config->item('app_name');
		
			$this->loadPage("profile", $this->global);
		}
		else
		{
			redirect("404");
		}
    }
	
    public function fund_raisers()
    {
        $this->global['pageTitle'] = 'Fund Raisers - ' . $this->config->item('app_name');
		
		$this->global['records'] = $this->frontend_model->get_records('tbl_service_request', "service = '4' and request_status = 'approved' and status = '0' order by id desc");
        
        $this->loadPage("fund-raisers", $this->global);
    }

	public function about_us()
    {
        $this->global['pageTitle'] = 'Fund Raisers - ' . $this->config->item('app_name');
		
		$this->global['records'] = $this->frontend_model->get_records('tbl_service_request', "service = '4' and request_status = 'approved' and status = '0' order by id desc");
        
        $this->loadPage("about-us", $this->global);
    }
	
    public function form_donate_now()
    {
        $data['result'] = 0;
		$info['fund_raiser_id'] = $this->input->post('fund_raiser_id');
		if(isset($_SESSION['is_login']))
		{
			$info['user_id'] = $_SESSION['login_id'];
		}
		$info['amount'] = $this->input->post('amount');
		$info['name'] = $this->input->post('name');
		$info['address'] = $this->input->post('address');
		$info['city'] = $this->input->post('city');
		$info['state'] = $this->input->post('state');
		$info['pincode'] = $this->input->post('pincode');
		$info['phone_number'] = $this->input->post('phone_number');
		$info['email'] = $this->input->post('email');
		$info['is_paid'] = 0;
		
		$payment = $this->request_payment($this->input->post('amount'), $this->input->post('phone_number'), $this->input->post('name'), $this->input->post('email'),"payment-result");
		
		$info['payment_request_id'] = $payment['payment_request_id'];
		
		if($this->frontend_model->insert('tbl_donors', $info))
		{
			$data['result'] = 1;
			$data['payment_url'] = $payment['longurl'];
		}
		
		echo json_encode($data);
    }

	public function payment_result_service_request(){
		$this->global['pageTitle'] = 'Payment Status - SPectra Sports';
        
		if(isset($_GET['payment_request_id']))
		{

			$pid = $_GET['payment_id'];
			$prid = $_GET['payment_request_id'];

			$ch = curl_init(); 

			curl_setopt($ch, CURLOPT_URL, "https://www.instamojo.com/api/1.1/payments/$pid/");
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER,
						array("X-Api-Key:c189322b52fa3a249200dda24fff95fa",
							  "X-Auth-Token:e6e6d6730817cab9e4ba60e3fb1ad07b"));

			$response = curl_exec($ch);
			curl_close($ch); 

			$exchangeT = json_decode($response, true);

			if($exchangeT['success'] == false)
			{
				//payment failed
			}else{
				if($exchangeT['payment']['status'] == 'Credit')
				{	
					$paid_by = $exchangeT['payment']['instrument_type'] . " - " . $exchangeT['payment']['billing_instrument'];
					
					$data1['is_paid'] = '1';
					$data1['payment_id'] = $pid;
					$data1['payment_method'] = $paid_by;
					$this->frontend_model->update('tbl_service_subscription', $data1, "status = '0' and payment_request_id='$prid'");
					// $this->global['tbl_service_payment-successsubscription'] = $this->global['tbl_service_subscription'][0];
					$this->loadPage("", $this->global);
				}
				else
				{
					$this->loadPage("payment-failure", $this->global);
				}
			}

		}else{
			$this->loadPage("payment-failure", $this->global);
		}
	}
	
	public function payment_result()
    {
        $this->global['pageTitle'] = 'Payment Status - SPectra Sports';
        
		if(isset($_GET['payment_request_id']))
		{
		
			$pid = $_GET['payment_id'];
			$prid = $_GET['payment_request_id'];

			$ch = curl_init(); 

			curl_setopt($ch, CURLOPT_URL, "https://www.instamojo.com/api/1.1/payments/$pid/");
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER,
						array("X-Api-Key:c189322b52fa3a249200dda24fff95fa",
							  "X-Auth-Token:e6e6d6730817cab9e4ba60e3fb1ad07b"));

			$response = curl_exec($ch);
			curl_close($ch); 

			$exchangeT = json_decode($response, true);

			if($exchangeT['success'] == false)
			{
				if(sizeof($this->frontend_model->get_records('tbl_donors', "payment_request_id='$prid'")) > 0)
				{
					$this->global['my_donation_details'] = $this->frontend_model->get_records('tbl_donors', "status = 0 and payment_request_id='$prid'")[0];
					$this->loadPage("payment-failure", $this->global);
				}
				else
				{
					$this->loadPage("payment-failure", $this->global);
				}
			}
			elseif($exchangeT['success'] == true)
			{       
				if($exchangeT['payment']['status'] == 'Credit')
				{	
					$paid_by = $exchangeT['payment']['instrument_type'] . " - " . $exchangeT['payment']['billing_instrument'];
					
					$data1['is_paid'] = '1';
					$data1['payment_id'] = $pid;
					$data1['payment_method'] = $paid_by;
					
					$this->global['my_donation_details'] = $this->frontend_model->get_records('tbl_donors', "status = 0 and payment_request_id='$prid'");
					
					$fund_raiser_id = $this->global['my_donation_details'][0]->fund_raiser_id;
					$fund_raiser = $this->frontend_model->get_records('tbl_service_request', "status = '0' and id='$fund_raiser_id'")[0];
					$data2['raised_amount'] = $fund_raiser->raised_amount + $this->global['my_donation_details'][0]->amount;
					if($data2['raised_amount'] >= $fund_raiser->fund_required)
					{
						$data2['request_status'] = "completed";
					}
					
					if(sizeof($this->global['my_donation_details']) > 0)
					{
						$this->global['my_donation_details'] = $this->global['my_donation_details'][0];
						$this->frontend_model->update('tbl_donors', $data1, "payment_request_id = '$prid'");
						$this->frontend_model->update('tbl_service_request', $data2, "status = '0' and id='$fund_raiser_id'");
						$this->loadPage("payment-success", $this->global);
					}
					else
					{
						$this->loadPage("payment-failure", $this->global);
					}
				}
				else
				{
					$this->loadPage("payment-failure", $this->global);
				}
			}
			else
			{
				$this->loadPage("payment-failure", $this->global);
			}
		}
		else
		{
			$this->loadPage("payment-failure", $this->global);
		}

    }
	
    function request_payment($total_amount, $mobile, $name, $email,$payEndPoint)
    {
        $ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER,
						array("X-Api-Key:c189322b52fa3a249200dda24fff95fa",
							  "X-Auth-Token:e6e6d6730817cab9e4ba60e3fb1ad07b"));
		$payload = Array(
			'purpose' => 'request from SpectraSports',
			'amount' => $total_amount,
			'phone' => $mobile,
			'buyer_name' => $name,
			'redirect_url' => base_url() . $payEndPoint,
			'send_email' => false,
			'webhook' => base_url() . $payEndPoint,
			'send_sms' => false,
			'email' => $email,
			'allow_repeated_payments' => false
		);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
		$response = curl_exec($ch);
		curl_close($ch); 

		$exchangeT = json_decode($response, true);

		if(isset($exchangeT['payment_request']['id']))
		{
			$data['longurl'] = $exchangeT['payment_request']['longurl'];
			$data['payment_request_id'] = $exchangeT['payment_request']['id'];
			return $data;
		}
		else
		{
			return false;
		}
    }
	
    public function fund_raiser($id)
    {
        $this->global['pageTitle'] = 'Fund Raiser - ' . $this->config->item('app_name');
		
		$this->global['record'] = $this->frontend_model->get_records('tbl_service_request', "id = '$id' and status = '0'");
        
		if(sizeof($this->global['record']) > 0)
		{
			$this->global['record'] = $this->global['record'][0];
			$this->loadPage("fund-raiser", $this->global);
		}
		else
		{
			redirect("404");
		}
    }
	
    public function events()
    {
        $this->global['pageTitle'] = 'Events - ' . $this->config->item('app_name');
		
		$this->global['records'] = $this->frontend_model->get_records('tbl_events', "status = '0' and event_status = 'approved' order by id desc");
        
        $this->loadPage("events", $this->global);
    }
	
    public function my_events()
    {
		if(!isset($_SESSION['is_login']))
		{
			redirect('/');
		}
        $this->global['pageTitle'] = 'My Events - ' . $this->config->item('app_name');
		
		$this->global['records'] = $this->frontend_model->get_records('tbl_events', "status = '0' and user_id = '$_SESSION[login_id]' order by id desc");
        
        $this->loadPage("my-events", $this->global);
    }



	public function my_subscription()
    {
		if(!isset($_SESSION['is_login']))
		{
			redirect('/');
		}
        $this->global['pageTitle'] = 'My Events - ' . $this->config->item('app_name');
		
		$this->global['records'] = $this->frontend_model->get_records('tbl_events', "status = '0' and user_id = '$_SESSION[login_id]' order by id desc");
        
        $this->loadPage("my-subscription", $this->global);
    }
	
    public function edit_event($id)
    {
		if(!isset($_SESSION['is_login']))
		{
			redirect('/');
		}
        
		$this->global['record'] = $this->frontend_model->get_records('tbl_events', "id = '$id' and user_id = '$_SESSION[login_id]' order by id desc")[0];
        
		$this->global['pageTitle'] = $this->global['record']->event_name . '- ' . $this->config->item('app_name'); 
		
        $this->loadPage("edit-event", $this->global);
    }
	
    public function edit_event_post()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$info['event_name'] = $this->input->post('event_name');
			$info['short_description'] = $this->input->post('short_description');
			$info['price'] = $this->input->post('price');
			$info['price_for'] = $this->input->post('price_for');
			$info['booking_link'] = $this->input->post('booking_link');
			$info['sport'] = $this->input->post('sport');
			$info['event_date'] = $this->input->post('event_date');
			$info['location'] = $this->input->post('location');
			$info['description'] = $this->input->post('description');
			foreach($_FILES as $key => $value)
			{
				$file_new_name = "spectra" . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
				if($this->image_upload($key, $file_new_name, 'uploads/') == true)
				{
					$info[$key] = $file_new_name;
				}
			}
			if($this->frontend_model->update('tbl_events', $info, "id = " . $this->input->post("event_id")))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function profile_image_form()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			foreach($_FILES as $key => $value)
			{
				$file_new_name = "spectra" . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
				if($this->image_upload($key, $file_new_name, 'uploads/') == true)
				{
					$info['profile_image'] = $file_new_name;
				}
			}
			if($this->frontend_model->update('tbl_general_users', $info, "id = " . $this->input->post("profile-id")))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function profile_banner_image_form()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			foreach($_FILES as $key => $value)
			{
				$file_new_name = "spectra" . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
				if($this->image_upload($key, $file_new_name, 'uploads/') == true)
				{
					$info['profile_banner_image'] = $file_new_name;
				}
			}
			if($this->frontend_model->update('tbl_general_users', $info, "id = " . $this->input->post("profile-id")))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function services()
    {
        $this->global['pageTitle'] = 'Services - ' . $this->config->item('app_name');
        
        $this->loadPage("services", $this->global);
    }

	public function paylist()
    {
        $this->global['pageTitle'] = 'paylist - ' . $this->config->item('app_name');
        
        $this->loadPage("paylist", $this->global);
    }
	public function subscription()
    {
        $this->global['pageTitle'] = 'subscription - ' . $this->config->item('app_name');
        
        $this->loadPage("subscription", $this->global);
    }
	
    public function services1($id)
    {
		$this->global['service'] = $this->frontend_model->get_records('tbl_all_services', "status = '0' and id = '$id'")[0];
		
        $this->global['pageTitle'] = $this->global['service']->name . ' - ' . $this->config->item('app_name');
        
        $this->loadPage("service_detail", $this->global);
    }
	
    public function apply_for_services($id, $name)
    {
		$services = $this->frontend_model->get_records('tbl_all_services', "status = '0' and id = '$id'");
		if(sizeof($services) > 0)
		{
			if(isset($_SESSION['is_login']))
			{
				$service = $services[0];
				
				$this->global['service'] = $service;
				
				$this->global['pageTitle'] = $service->name . ' - ' . $this->config->item('app_name');
				
				$this->loadPage("apply-for-services", $this->global);
			}
			else
			{
				redirect('404');
			}
		}
		else
		{
			redirect('404');
		}
    }
	
    function service_new_request()
    {
        // print_r($_SESSION['is_login']);
        if(false)
        {
            $data['result'] = 2;
        }
        else
        {
			foreach($_POST as $key => $value)
			{
				$info[$key] = $value;
			}
			
			// $info['user_id'] = $_SESSION['login_id'];
			$info['user_id'] = 1;
			
			$table = "tbl_service_subscription";
			
			$folder_name = 'service-requests';

			$info['is_paid'] = 0;

			if($info['raised_amount']!=0){
				$payment = $this->request_payment($info['raised_amount'], '', '', '',"payment-result-service-request");
				$info['payment_request_id'] = $payment['payment_request_id'];
                $data['payment_url'] = $payment['longurl'];
            }else{
				$info['is_paid'] = 1;
                $data['payment_url'] = null;
            }
			
			
			if(isset($_FILES['proof_upload']['name'])&&$_FILES['proof_upload']['name'])
			{
				$file_new_name = date('Ydm') . time() . uniqid() . "." . strtolower(pathinfo($_FILES["proof_upload"]["name"],PATHINFO_EXTENSION));
				if($this->image_upload("proof_upload", $file_new_name, 'uploads/' . $folder_name . '/') == true)
				{
					$info["proof_upload"] = $file_new_name;
				}
				
				if($insert_id = $this->frontend_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 3;
				}
			}
			else
			{
				if($insert_id = $this->frontend_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 0;
				}
			}
			
			if(isset($_FILES['resume_upload']['name'])&&$_FILES['resume_upload']['name'])
			{
				$file_new_name = date('Ydm') . time() . uniqid() . "." . strtolower(pathinfo($_FILES["resume_upload"]["name"],PATHINFO_EXTENSION));
				if($this->image_upload("resume_upload", $file_new_name, 'uploads/' . $folder_name . '/') == true)
				{
					$info["resume_upload"] = $file_new_name;
				}
				
				if($insert_id = $this->frontend_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 3;
				}
			}
			else
			{
				if($insert_id = $this->frontend_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 0;
				}
			}
        }
		echo json_encode($data);
    }
	
    public function login()
    {
		if(!isset($_SESSION['is_login']))
		{
			$this->global['pageTitle'] = 'Login - ' . $this->config->item('app_name');
			
			$this->global['msg'] = "";
			if($this->session->flashdata('msg'))
			{
				$this->global['msg'] = $this->session->flashdata('msg');
			}
			
			$this->loadPage("login", $this->global);
		}
		else
		{
			redirect('my-profile');
		}
    }
	
    public function logout()
    {
		unset($_SESSION['is_login']);
		unset($_SESSION['login_id']);
		redirect(base_url());
    }
	
    public function loginPost()
    {
        if(!empty($this->input->post('phone')))
		{			
			$phone_number = $this->input->post('phone');
			$password = md5($this->input->post('password'));
			$records = $this->frontend_model->get_records('tbl_general_users', "phone_number = '" . $phone_number . "' and password = '" . $password . "'");
			if(sizeof($records) > 0)
			{
				if($records[0]->status == 0)
				{
					$_SESSION['is_login'] = true;
					$_SESSION['login_id'] = $records[0]->id;
					$data['result'] = 1;
				}
				else
				{
					$data['result'] = 2;
				}
			}
			else
			{
				$data['result'] = 0;
			}
		}
		else
		{
			$data['result'] = 0;
		}
		echo json_encode($data);
    }
	
    public function registerPost()
    {
		$success=false;
        if(!empty($this->input->post('phone_number')))
		{
			if($this->input->post('password') == $this->input->post('confirm_password'))
			{
				$info['first_name'] = $this->input->post('first_name');
				$info['last_name'] = $this->input->post('last_name');
				$info['phone_number'] = $this->input->post('phone_number');
				$info['service'] = $this->input->post('service');
				$info['email'] = $this->input->post('email');
				if($this->input->post('organization_name'))
				{
					$info['organization_name'] = $this->input->post('organization_name');
				}
				$info['password'] = md5($this->input->post('password'));
				if(sizeof($this->frontend_model->get_records('tbl_general_users', "phone_number = '" . $this->input->post('phone_number') . "'")) > 0)
				{
					$data['result'] = 2;
				}
				else
				{
					if($insert_id = $this->frontend_model->insert('tbl_general_users', $info))
					{
						$success=true;
						$_SESSION['is_login'] = true;
						$_SESSION['login_id'] = $insert_id;
						$data['result'] = 1;
					}
					else
					{
						$data['result'] = 0;
					}
				}
			}
			else
			{
				$data['result'] = 0;
			}
		}
		else
		{
			$data['result'] = 0;
		}
		echo json_encode($data);
		if($success){
			$this->sendEmail($info['email']);
		}
    }
	
    public function member_follow_form()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$info['request_from'] = $_SESSION['login_id'];
			$info['request_to'] = $this->input->post('user_member');
			$info['type'] = $this->input->post('member_type');
			$info['acceptance'] = $this->input->post('yes-no');
			
			if($this->frontend_model->insert('tbl_friend_request', $info))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function update_my_profile()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$info['first_name'] = $this->input->post('first_name');
			$info['last_name'] = $this->input->post('last_name');
			$info['short_bio'] = $this->input->post('short_bio');
			$info['interested_sport'] = $this->input->post('interested_sport');
			// $info['service'] = $this->input->post('service');
			$info['date_of_birth'] = $this->input->post('date_of_birth');
			$info['gender'] = $this->input->post('gender');
			$info['address'] = $this->input->post('address');
			$info['city'] = $this->input->post('city');
			$info['state'] = $this->input->post('state');
			$info['pincode'] = $this->input->post('pincode');
			if($this->frontend_model->update('tbl_general_users', $info, "id=" . $_SESSION['login_id']))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function my_profile_change_password()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$pass = md5($this->input->post('currentpassword'));
			if(sizeof($this->frontend_model->get_records("tbl_general_users", "status = '0' and id = '$_SESSION[login_id]' and password = '$pass'")) > 0)
			{
				if($this->input->post('password') == $this->input->post('confirm_password'))
				{
					$info['password'] = md5($this->input->post('password'));
					if($this->frontend_model->update('tbl_general_users', $info, "id=" . $_SESSION['login_id']))
					{
						$data['result'] = 1;
					}
				}
			}
		}
		echo json_encode($data);
    }
	
    public function post_a_event()
    {
		if(!isset($_SESSION['is_login']))
		{
			redirect('/');
		}
		$this->global['pageTitle'] = 'Post a event - ' . $this->config->item('app_name');
        
        $this->loadPage("post-a-event", $this->global);
    }
	
    public function new_event()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$info['event_name'] = $this->input->post('event_name');
			$info['short_description'] = $this->input->post('short_description');
			$info['price'] = $this->input->post('price');
			$info['price_for'] = $this->input->post('price_for');
			$info['booking_link'] = $this->input->post('booking_link');
			$info['sport'] = $this->input->post('sport');
			$info['event_date'] = $this->input->post('event_date');
			$info['location'] = $this->input->post('location');
			$info['description'] = $this->input->post('description');
			$info['user_id'] = $_SESSION['login_id'];
			foreach($_FILES as $key => $value)
			{
				$file_new_name = "spectra" . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
				if($this->image_upload($key, $file_new_name, 'uploads/') == true)
				{
					$info[$key] = $file_new_name;
				}
			}
			if($this->frontend_model->insert('tbl_events', $info))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function add_acheivements()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$info['title'] = $this->input->post('acheivement-award');
			$info['position'] = $this->input->post('position');
			$info['issued_by'] = $this->input->post('issuer');
			$info['date'] = $this->input->post('date');
			$info['place'] = $this->input->post('place');
			$info['user_id'] = $_SESSION['login_id'];
			foreach($_FILES as $key => $value)
			{
				$file_new_name = "spectra" . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
				if($this->image_upload($key, $file_new_name, 'uploads/') == true)
				{
					$info['proof_image'] = $file_new_name;
				}
			}
			if($this->frontend_model->insert('tbl_user_acheivements', $info))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
    public function user_gallery_images_upload()
    {
		if(!isset($_SESSION['is_login']))
		{
			$data['result'] = 0;
		}
		else
		{
			$data['result'] = 0;
			$info['user_id'] = $_SESSION['login_id'];
			//print_r($_FILES);
			for($i = 0; $i < sizeof($_FILES['gallery_image']['name']); $i++)
			{
				$file_new_name = "spectra" . uniqid() . "." . strtolower(pathinfo($_FILES['gallery_image']["name"][$i],PATHINFO_EXTENSION));
				if($this->image_upload1('gallery_image', $file_new_name, 'uploads/', $i) == true)
				{
					$info['image'] = $file_new_name;
				}
			}
			if($this->frontend_model->insert('tbl_user_gallery', $info))
			{
				$data['result'] = 1;
			}
		}
		echo json_encode($data);
    }
	
	function get_fixtures()
	{
		$result = file_get_contents("https://cricket.sportmonks.com/api/v2.0/fixtures?api_token=BinfEUouWBoWOU6Md70ILH3Be8lZV9iYqOS6jmkuKoqrNVVfvw1vxbOGSbBK&page[from]=" . date('Y-m-d'));
		$result = json_decode($result, true);
		return $result['data'];
	}
	
	function get_team_by_id($id)
	{
		$result = file_get_contents("https://cricket.sportmonks.com/api/v2.0/teams/$id?api_token=BinfEUouWBoWOU6Md70ILH3Be8lZV9iYqOS6jmkuKoqrNVVfvw1vxbOGSbBK");
		$result = json_decode($result, true);
		return $result['data'];
	}
	
    function pageNotFound()
    {
        $this->global['pageTitle'] = '404 - Page Not Found - ' . $this->config->item('app_name');
        
        $this->loadPage("404", $this->global);
    }
	
	function image_upload($atr_name, $file_new_name, $target_dir)
	{
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($_FILES[$atr_name]["name"],PATHINFO_EXTENSION));
		$target_file = $target_dir . $file_new_name;
		if (file_exists($target_file)) {
			$uploadOk = 0;
		}
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$uploadOk = 0;
		}
		if ($uploadOk == 0) {
			return false;
		} else {
			if (move_uploaded_file($_FILES[$atr_name]["tmp_name"], $target_file)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	function image_upload1($atr_name, $file_new_name, $target_dir, $i)
	{
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($_FILES[$atr_name]["name"][$i],PATHINFO_EXTENSION));
		$target_file = $target_dir . $file_new_name;
		if (file_exists($target_file)) {
			$uploadOk = 0;
		}
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$uploadOk = 0;
		}
		if ($uploadOk == 0) {
			return false;
		} else {
			if (move_uploaded_file($_FILES[$atr_name]["tmp_name"][$i], $target_file)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public static function slugify($text)
	{
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}
}

?>