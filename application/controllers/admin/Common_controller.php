<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';


class Common_controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/common_model');
        $this->isLoggedIn();   
		
		$this->global['services'] = $this->common_model->get_records('tbl_services', "status = '0'");
		
		$this->global['tis'] = $this;
    }
    
    public function index()
    {
        $this->global['pageTitle'] = $this->config->item('app_name') . ' : ' . 'Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    function new_feed()
    {
        $this->global['pageTitle'] =  'Create New Feed' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_news_category', "status = '0'");
		
        $this->loadViews("new-feed", $this->global, NULL , NULL);
    }
    
    function all_feeds()
    {
        $this->global['pageTitle'] =  'All Feeds' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_news', "status = '0' order by id desc");
		
        $this->loadViews("list-feeds", $this->global, NULL , NULL);
    }
    function all_teams()
    {
        $this->global['pageTitle'] =  'All Teams' . " - " . $this->config->item('app_name');
        
		
        $this->loadViews("all-teams", $this->global, NULL , NULL);
    }
	function all_idols()
    {
        $this->global['pageTitle'] =  'All Feeds' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_news', "status = '0' order by id desc");
		
        $this->loadViews("all-idols", $this->global, NULL , NULL);
    }

    function all_additional_service()
    {
        $this->global['pageTitle'] =  'All Additional Service' . " - " . $this->config->item('app_name');
        
		// $this->global['records'] = $this->common_model->get_records('tbl_news', "status = '0' order by id desc");
		
        $this->loadViews("all-additional-service", $this->global, NULL , NULL);
    }

    function add_additional_service()
    {
        $this->global['pageTitle'] =  'All Additional Service' . " - " . $this->config->item('app_name');
        
		// $this->global['records'] = $this->common_model->get_records('tbl_news', "status = '0' order by id desc");
		
        $this->loadViews("add-additional-service", $this->global, NULL , NULL);
    }

	function all_feedback()
    {
        $this->global['pageTitle'] =  'All Feeds' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_news', "status = '0' order by id desc");
		
        $this->loadViews("all-feedback", $this->global, NULL , NULL);
    }


	function membership()
    {
        $this->global['pageTitle'] =  'All Feeds' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_news', "status = '0' order by id desc");
		
        $this->loadViews("membership", $this->global, NULL , NULL);
    }

	function add_idols()
    {
        $this->global['pageTitle'] =  'Create New Feed' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_news_category', "status = '0'");
		
        $this->loadViews("add-idols", $this->global, NULL , NULL);
    }
	function add_feedback()
    {
        $this->global['pageTitle'] =  'Create New Feed' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_news_category', "status = '0'");
		
        $this->loadViews("add-feedback", $this->global, NULL , NULL);
    }

	function add_teams()
    {
        $this->global['pageTitle'] =  'Add Team' . " - " . $this->config->item('app_name');
        
		
        $this->loadViews("add-teams", $this->global, NULL , NULL);
    }

	function edit_idols($id)
    {
		$this->global['pageTitle'] =  'Create New Feed' . " - " . $this->config->item('app_name');

		 
        
		$this->global['record'] = $this->common_model->get_records('tbl_idols', "id = '$id'")[0];
		
        $this->loadViews("edit-idols", $this->global, NULL , NULL);

    }


	function edit_feedback($id)
    {
		$this->global['pageTitle'] =  'Create New Feed' . " - " . $this->config->item('app_name');

		$this->global['record'] = $this->common_model->get_records('tbl_feedback', "id = '$id'")[0];
		
        $this->loadViews("edit-feedback", $this->global, NULL , NULL);      
    }

	function edit_teams($id)
    {
		$this->global['pageTitle'] =  'Edit Teams' . " - " . $this->config->item('app_name');

		$this->global['record'] = $this->common_model->get_records('tbl_teams', "id = '$id'")[0];
		
        $this->loadViews("edit-teams", $this->global, NULL , NULL);      
    }

    function edit_additional_service($id)
    {
		$this->global['pageTitle'] =  'Edit Additional Service' . " - " . $this->config->item('app_name');

        $this->global['record'] = $this->common_model->get_records('tbl_all_services', "id = '$id'")[0];
    		
        $this->loadViews("edit-additional-service", $this->global, NULL , NULL);      
		 
	       
    }
   
	function edit_membership($id)
    {
		$this->global['pageTitle'] =  'Create New Feed' . " - " . $this->config->item('app_name');

		 
        
		$this->global['record'] = $this->common_model->get_records('tbl_membership', "id = '$id'")[0];
		
        $this->loadViews("edit-membership", $this->global, NULL , NULL);      
		 
	       
    }






    function edit_feed($id)
    {
		$records = $this->common_model->get_records("tbl_news", "status = '0' and id = '$id'");
		if(sizeof($records))
		{
			$this->global['pageTitle'] =  $records[0]->title . " - " . $this->config->item('app_name');
			
			$this->global['cats'] = $this->common_model->get_records('tbl_news_category', "status = '0' order by id desc");
			$this->global['record'] = $records[0];
			
			$this->loadViews("edit-feed", $this->global, NULL , NULL);
		}
    }
	 
    
    function view_service_request($id)
    {
        $this->global['pageTitle'] =  'Service Request' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_service_request', "status = '0' and id = '$id'");
		
        $this->loadViews("view-service-request", $this->global, NULL , NULL);
    }
    
    function edit_service_request($id)
    {
        $this->global['pageTitle'] =  'Service Request' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_service_request', "status = '0' and id = '$id'");
		
        $this->loadViews("edit-service-request", $this->global, NULL , NULL);
    }
    
    function new_cf()
    {
        $this->global['pageTitle'] =  'Create Crowd Fund' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_cf_category', "status = '0'");
		
        $this->loadViews("new-cf", $this->global, NULL , NULL);
    }
    
    function all_cf()
    {
        $this->global['pageTitle'] =  'All Crowd Funds' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_cf', "status = '0' order by id desc");
		
        $this->loadViews("list-cf", $this->global, NULL , NULL);
    }
    
    function edit_cf($id)
    {
		$records = $this->common_model->get_records("tbl_cf", "status = '0' and id = '$id'");
		if(sizeof($records))
		{
			$this->global['pageTitle'] =  $records[0]->title . " - " . $this->config->item('app_name');
			
			$this->global['cats'] = $this->common_model->get_records('tbl_cf_category', "status = '0' order by id desc");
			$this->global['record'] = $records[0];
			
			$this->loadViews("edit-cf", $this->global, NULL , NULL);
		}
    }
    
    function list_users($id)
    {
        $this->global['pageTitle'] =  $this->common_model->get_record('tbl_services', "id = '$id'", "name") . ' - General Users' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_general_users', "service = '$id' order by id desc");
		
		$this->global['service_id'] = $id;
		
        $this->loadViews("list-users", $this->global, NULL , NULL);
    }
    
    function list_service_request($id)
    {
        $this->global['pageTitle'] =  $this->common_model->get_record('tbl_all_services', "id = '$id'", "name") . ' - Service Requests' . " - " . $this->config->item('app_name');
        
		$this->global['service_id'] = $id;
		$this->global['records'] = $this->common_model->get_records('tbl_service_request', "service = '$id' order by id desc");
		
        $this->loadViews("list-service-request", $this->global, NULL , NULL);
    }
    
    function donors()
    {
        $this->global['pageTitle'] =  'Donors' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_donors', "status = '0' and is_paid = '1' order by id desc");
		
        $this->loadViews("donors", $this->global, NULL , NULL);
    }
    
    function events()
    {
        $this->global['pageTitle'] =  'Events' . " - " . $this->config->item('app_name');
        
		$this->global['records'] = $this->common_model->get_records('tbl_events', "status = '0' order by id desc");
		
        $this->loadViews("list-events", $this->global, NULL , NULL);
    }
    
    function edit_event($id)
    {
        $this->global['pageTitle'] =  'Events' . " - " . $this->config->item('app_name');
        
		$this->global['record'] = $this->common_model->get_records('tbl_events', "status = '0' and id = '$id'")[0];
		
        $this->loadViews("edit-event", $this->global, NULL , NULL);
    }
    
	function insert1()
    {
        if($this->isAdmin() == TRUE)
        {
            $data['result'] = 0;
        }
        else
        {
			foreach($_POST as $key => $value)
			{
				if($key != 'table_name' && $key != 'row_id')
				{
					$info[$key] = $value;
				}
			}
			
			$table = $this->input->post('table_name');
			
			$folder_name = 'common';
			$path = "uploads/";
			
			if($_POST['table_name'] == "tbl_idols")
			{
				$path = "uploads/idols/";
			}
			
			 if($table == "tbl_idols")
			 {
			 	$path = 'uploads/news/'; 
			 }
			
			if(sizeof($_FILES) > 0)
			{
				
				foreach($_FILES as $key => $value)
				{
					$file_new_name = date('Ydm') . time() . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
					if($this->image_upload($key, $file_new_name, $path) == true)
					{
						$info[$key] = $file_new_name;
					}
				}
				
				if($insert_id = $this->common_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 0;
				}
			}
			else
			{
				if($insert_id = $this->common_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 0;
				}
			}
        }
		echo json_encode($data);
    }
	





    function insert()
    {
        if($this->isAdmin() == TRUE)
        {
            $data['result'] = 0;
        }
        else
        {
			foreach($_POST as $key => $value)
			{
				if($key != 'table_name' && $key != 'row_id')
				{
					$info[$key] = $value;
				}
			}
			
			$table = $this->input->post('table_name');
			
			$folder_name = 'common';
			$path = "uploads/";
			
			if($_POST['table_name'] == "tbl_service_request")
			{
				$path = "uploads/service-requests/";
			}
			
			if($table == "tbl_news")
			{
				$path = 'uploads/news/'; 
			}
			
			if(sizeof($_FILES) > 0)
			{
				
				foreach($_FILES as $key => $value)
				{
					$file_new_name = date('Ydm') . time() . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
					if($this->image_upload($key, $file_new_name, $path) == true)
					{
						$info[$key] = $file_new_name;
					}
				}
				
				if($insert_id = $this->common_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 0;
				}
			}
			else
			{
				if($insert_id = $this->common_model->insert($table, $info))
				{
					$data['result'] = 1;
					$data['insert_id'] = $insert_id;
				}
				else
				{
					$data['result'] = 0;
				}
			}
        }
		echo json_encode($data);
    }
	
    function update()
    {
        if($this->isAdmin() == TRUE)
        {
            $data['result'] = 0;
        }
        else
        {
			$info = array();
			foreach($_POST as $key => $value)
			{
				if($key != 'table_name' && $key != 'row_id')
				{
					$info[$key] = $value;
				}
			}
			
			$table = $this->input->post('table_name');
			$row_id = $this->input->post('row_id');
			
			$folder_name = "uploads/";
			
			if($_POST['table_name'] == "tbl_service_request")
			{
				$folder_name = "uploads/service-requests/";
			}
			
			if($table == "tbl_news")
			{
				$folder_name = 'uploads/news/';
			}
			
			if(sizeof($_FILES) > 0)
			{
				foreach($_FILES as $key => $value)
				{
					$file_new_name = date('Ydm') . time() . uniqid() . "." . strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
					if($this->image_upload($key, $file_new_name, $folder_name) == true)
					{
						$info[$key] = $file_new_name;
					}
				}
				if($this->common_model->update($table, $info, "id = '" . $row_id . "'"))
				{
					$data['result'] = 1;
				}
				else
				{
					$data['result'] = 0;
				}
			}
			else
			{
				if($this->common_model->update($table, $info, "id = '" . $row_id . "'"))
				{
					$data['result'] = 1;
				}
				else
				{
					$data['result'] = 0;
				}
			}
			
        }
		echo json_encode($data);
    }
	
	function image_upload($atr_name, $file_new_name, $target_dir)
	{
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($_FILES[$atr_name]["name"],PATHINFO_EXTENSION));
		$target_file = $target_dir . $file_new_name;
		if (file_exists($target_file)) {
			$uploadOk = 0;
		}
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$uploadOk = 0;
		}
		if ($uploadOk == 0) {
			return false;
		} else {
			if (move_uploaded_file($_FILES[$atr_name]["tmp_name"], $target_file)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	function slugify($text)
	{
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}
}

?>