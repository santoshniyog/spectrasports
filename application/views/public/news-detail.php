<main id="content" class="mt--10">
    <div class="container">
        <div class="row mt--10">
            <!-- top section -->
            <div class="col-12 mt-05 mt--10">
                <!-- Big grid slider 1 -->
                <div class="row featured-1 mb-5 mt--10">

                    <!-- start left column -->
                    <div class="col-md-8">
                        <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                            <div class="blog-thumb mb-30">
                                <img src="<?=base_url()?>uploads/news/<?=$news_detail->image?>" alt="<?=$news_detail->title?>">
                            </div>
                            <div class="blog-content">
                                <div class="post-meta">
                                    <a href="javascript:void(0)"><?=date("d M, Y", strtotime($news_detail->date))?></a>
                                    <a href="<?=base_url()?>post/<?=$news_detail->category?>/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "status = '0' and id = '" . $news_detail->category . "'", "name"))?>">
										<?=$this->frontend_model->get_record("tbl_news_category", "status = '0' and id = '" . $news_detail->category . "'", "name")?>
									</a>
                                </div>
                                <h4 class="post-title">
									<?=$news_detail->title?>
								</h4>

                                <div class="post-meta-2">
                                    <a href="#">
										<i class="fa fa-thumbs-up" aria-hidden="true"></i>
										<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news_detail->id . "'"))?>
									</a>
                                </div>
                                
								<?=$news_detail->description?>
								
                            </div>
                        </div>
                    </div>
                    <!-- end left column -->

                    <?php include("new-right-sidebar.php"); ?>
					
                </div>
            </div>
        </div>
    </div>
</main>