
	<div class="box">
		<div class="sticky">
			<!--Social network 2-->
			<aside class="widget">
				<!-- widget title -->
				<div class="block-title-4">
					<h4 class="h5 title-arrow">
						<span>Social network</span>
					</h4>
				</div>
				<!-- widget content -->
				<ul class="list-unstyled social-two">
					<li class="facebook">
						<a class="bg-facebook text-white" href="javascript:void(0);" target="_blank" title="facebook">
							Facebook
						</a>
					</li>
					<li class="twitter">
						<a class="bg-twitter text-white" href="javascript:void(0);" target="_blank" title="twitter">
							Twitter
						</a>
					</li>
					<li class="instagram">
						<a class="bg-instagram text-white" href="javascript:void(0);" target="_blank" title="instagram">
							Instagram
						</a>
					</li>
					<li class="linkedin">
						<a class="bg-linkedin text-white" href="javascript:void(0);" target="_blank" title="linkedin">
							Linkedin
						</a>
					</li>
				</ul>
				<!-- end widget content -->
				<div class="gap-15"></div>
			</aside>

			<!-- post with number -->
			<aside class="widget">
				<!--block title-->
				<div class="block-title-4">
					<h4 class="h5 title-arrow">
						<span>
							<?=$this->frontend_model->get_record("tbl_news_category", "id = 8", "name");?>
						</span>
					</h4>
				</div>
				<!-- Block content -->
				<ul class="post-number list-unstyled border-bottom-last-0 rounded mb-3">
					<?php 
						foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '8' order by id desc limit 5") as $news):
						$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
						if($news->redirect_link != "")
						{
							$link = $news->redirect_link;
						}
					?>
						<li class="hover-a">
							<a class="h5 h6-md h5-lg" href="<?=$link?>">
								<?=$news->title?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
				<!-- end block content -->
				<div class="gap-0"></div>
			</aside>
			<!-- end post with number -->
			
			<!-- latest post -->
			<aside class="widget">
				<!--Block title-->
				<div class="block-title-4">
					<h4 class="h5 title-arrow">
						<span>
							<?=$this->frontend_model->get_record("tbl_news_category", "id = 9", "name");?>
						</span>
					</h4>
				</div>
				<!--post big start-->
				<div class="big-post">
					<?php 
						foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '9' order by date desc limit 1") as $news):
					?>
						<article class="card card-full hover-a mb-4">
							<!--thumbnail-->
							<div class="ratio_360-202 image-wrapper">
								<a href="<?=base_url()?>post/<?=$news->id?>/<?=$tis->slugify($news->title)?>">
									<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
								</a>
							</div>
							<div class="card-body">
								<!--title-->
								<h2 class="card-title h1-sm h3-md">
									<a href="<?=base_url()?>post/<?=$news->id?>/<?=$tis->slugify($news->title)?>">
										<?=$news->title?>
									</a>
								</h2>
								<!-- author & date -->
								<div class="card-text mb-2 text-muted small">
									<time datetime="<?=$news->date?>">
										<?=date("d M, Y", strtotime($news->date))?>
									</time>
								</div>
								<!--description-->
								<p class="card-text">
									<?=$news->short_description?>
								</p>
							</div>
						</article>
					<?php endforeach; ?>
				</div>
				<!--end post big-->
				<!--post small start-->
				<div class="small-post">
					<?php 
						foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '9' order by date desc limit 2, 3") as $news):
					?>
						<!--post list-->
						<article class="card card-full hover-a mb-4">
							<div class="row">
								<!--thumbnail-->
								<div class="col-3 col-md-4 pr-2 pr-md-0">
									<div class="ratio_110-77 image-wrapper">
										<a href="<?=base_url()?>post/<?=$news->id?>/<?=$tis->slugify($news->title)?>">
											<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
										</a>
									</div>
								</div>
								<!-- title & date -->
								<div class="col-9 col-md-8">
									<div class="card-body pt-0">
										<h3 class="card-title h6 h5-sm h6-md">
											<a href="<?=base_url()?>post/<?=$news->id?>/<?=$tis->slugify($news->title)?>">
												<?=$news->title?>
											</a>
										</h3>
										<div class="card-text small text-muted">
											<time datetime="<?=$news->date?>">
												<?=date("d M, Y", strtotime($news->date))?>
											</time>
										</div>
									</div>
								</div>
							</div>
						</article>
						<!--End post list-->
					<?php endforeach; ?>
				</div>
				<!--end post small-->
				<div class="gap-0"></div>
			</aside>
			<!-- end latest post -->

			
			
		</div>
	</div>