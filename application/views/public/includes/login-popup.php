
		<div id="login" class="modal fade" role="dialog">
			<div class="modal-dialog">	
				<div class="modal-content">
					<div class="modal-body">
					<button data-dismiss="modal" class="close">×</button>
					<div class="col2-set" id="customer_login2">
						<div class="col-lg-12">
							<div class="form-design">
								<h4>Login</h4>
								<hr>
								<form method="post" class="login form_login">
									<p class="form-row form-row-wide">
										<label for="phone">Mobile Number <span class="required">*</span></label>
										<input type="number" class="form-control" name="phone" id="phone" value="" required />
									</p>
									<p class="form-row form-row-wide">
										<label for="password">Password <span class="required">*</span></label>
										<input class="form-control" type="password" name="password" id="password" required />
									</p>
									<p class="form-row form-row-wide">
										<button type="submit" class="button pull-right btn btn-danger">Login</button>											
									</p>
								</form>
								<hr>
								<small><a href="javascript:void(0);" data-target="#forget-password" data-toggle="modal" >Forget your password?</a></small><br>
								<small><a href="javascript:void(0);" data-target="#register" data-toggle="modal">New to Spectra Sports? Register!</a></small>
								
							</div>
						</div>
					</div>
					</div>
				</div>  
			</div>  
		</div>
	