	
		<div id="forget-password" class="modal fade" role="dialog">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-body">
				<button data-dismiss="modal" class="close">×</button>
				<div class="col2-set" id="customer_login2">
					<div class="col-lg-12">
						<div class="form-design">
							<h4>Forget Password</h4>
							<hr>
							<form method="post" class="register form_register">
								
								<p class="form-row form-row-wide">
									<label for="last_name">Email <span class="required">*</span></label>
									<input type="text" class="form-control" name="email" id="email" required/>
								</p>
								
								
								<p class="form-row form-row-wide">
									<button type="submit" class="button pull-right btn btn-danger">Submit</button>
								</p>
							</form>	
							<hr>
							<small><a href="<?=base_url()?>login">Have an account? Login here.</a></small><br>
									
						</div>
					</div>
				</div>
			  </div>
			</div>
		  </div>  
		</div>
		