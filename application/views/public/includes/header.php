<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />

        <title><?=$pageTitle?></title>
        <meta content="" name="descriptison" />
        <meta content="" name="keywords" />

        <!-- Favicons -->
        <link href="<?=base_url()?>assets/front/img/favicon.png" rel="icon" />
        <link href="<?=base_url()?>assets/front/img/apple-touch-icon.png" rel="apple-touch-icon" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet" />

        <!-- Vendor CSS Files -->
        <link href="<?=base_url()?>assets/front/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/front/vendor/icofont/icofont.min.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/front/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/front/vendor/venobox/venobox.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/front/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/front/vendor/aos/aos.css" rel="stylesheet" />
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="<?=base_url()?>assets/front/css/style.css" rel="stylesheet" />
		<script src="<?=base_url()?>assets/front/vendor/jquery/jquery.min.js"></script>
		
		<?php if(isset($news_page)): ?>
			<link rel="stylesheet" href="<?=base_url()?>assets/front/news/assets/css/theme.css">
			<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/news/assets/vendor/bootstrap-customizer/css/bootstrap-customizer.css">
		<?php endif; ?>
		
		<script>
			var baseURL = '<?=base_url()?>';
		</script>
    </head>

    <body>
        <!-- ======= Top Bar ======= -->
        <section id="topbar" class="d-none d-lg-block">
            <div class="container d-flex">
                <div class="contact-info mr-auto">
                    <ul>
                        <li><i class="icofont-envelope"></i><a href="mailto:admin@spectrasports.in">admin@spectrasports.in</a></li>
                        <li><i class="icofont-phone"></i> +91-9840802580</li>
                        <li><i class="icofont-clock-time icofont-flip-horizontal"></i> Mon-Fri 9am - 5pm</li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- ======= Header ======= -->
        <header id="header">
            <div class="container d-flex">
                <div class="logo mr-auto">
                    <h1 class="text-light">
                        <a href="<?=base_url()?>"><img src="<?=base_url()?>assets/front/img/spectra-logo.png" /></a>
                    </h1>
                    <!-- Uncomment below if you prefer to use an image logo -->
                </div>

                <nav class="nav-menu d-none d-lg-block">
                    <ul class="nav">
                        <li><a class=""  href="<?=base_url()?>">Home</a></li>
                        <li><a class="" href="<?=base_url()?>about-us">About</a></li>
                        <li><a class="" href="<?=base_url()?>services">Services</a></li>
                        <li><a class="" href="<?=base_url()?>events">Events</a></li>
                        <!-- <li><a class="" href="<?=base_url()?>home"> News</a></li> -->
                        <!-- <li><a class="" href="<?=base_url()?>fund-raisers">Browse Fundraisers</a></li> -->
						<?php if(isset($_SESSION['is_login'])): ?>
						<li class="drop-down"><a href="#">My Account</a>
							<ul>
								<li><a href="<?=base_url()?>my-account">Account</a></li>
								<li><a href="<?=base_url()?>my-profile">My profile</a></li>
								<li><a href="<?=base_url()?>post-a-event">Post a event</a></li>
                                <li><a href="<?=base_url()?>my-subscription">My subscription</a></li>
								<li><a href="<?=base_url()?>my-events">My Events</a></li>
								<li><a href="<?=base_url()?>logout">Logout</a></li>
							</ul>
						</li>
						<?php else: ?>
                        <li><a href="javascript:void(0);" data-target="#register" data-toggle="modal">Register</a></li>
                        <li><a href="javascript:void(0);" data-target="#login" data-toggle="modal">Login</a></li>
						<?php endif; ?>
                    </ul>
                </nav>
                <!-- .nav-menu -->
            </div>
        </header>
        <!-- End Header -->
