<main id="main">

    <section id="why-us" class="why-us">
      <div class="container">
		<div class="row">
			<div class="col-xl-12 d-flex align-items-stretch">
				<div class="row">
					<?php $i = 1; ?>
					<?php foreach($records as $event): ?>
				
						<div class="col-md-4 mb-2">
							<div class="card dynamic_events">
							<div>
							<img class="card-img-top" src="<?=base_url()?>uploads/<?=$event->event_image?>" alt="<?=base_url()?>uploads/<?=$event->event_image?>" />
							<div class="pos_date">
                              <span class="date"> <?= date('d', strtotime($event->event_date)) ?></span>
                              <div class="month_pos">
                              <span> <?= date('M', strtotime($event->event_date)) ?></span>
                              <span> <?= date('Y', strtotime($event->event_date)) ?></span>
                              </div>
                           
                           </div>
							</div>
							<div class="card-body">
                           <h5 class="card-title"><?= $event->event_name ?></h5>
                          

                           <ul class="list-group list-group-flush blog_ul">
                              <li class="list-group-item"><i class="fa fa-icon fa-map-marker"></i> <span><?= $event->location ?></span> </li>
                              <li class="list-group-item">₹<?= $event->price ?>/- <?= $event->price_for ?></li>
                           </ul>
                           <p class="card-text font-13" style="text-overflow: ellipsis;"><?= $event->short_description ?></p>
                        </div>
								<div class="card-body d_flex">
									<a href="<?=base_url()?>event/<?=$event->id?>/<?=$tis->slugify($event->event_name)?>" class="card-link">Know More</a>
									<a target="_blank" href="<?=$event->booking_link?>" class="card-link btn btn-sm btn-secondary">Book Now</a>
								</div>
							</div>
						</div>
						<?php if($i%50 == 0 && $i != sizeof($records)): ?>
							</div>
							<div class="row"> 
						<?php endif; ?>
						<?php $i++; ?>
					<?php endforeach; ?>
				</div>
			</div>
        </div>

      </div>
    </section>

</main>
