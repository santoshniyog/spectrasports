<main id="content" class="mt--10">
    <div class="container-fluid">
        <div class="row">
            <!-- top section -->
            <div class="col-12 mb-4">
                <!-- Block start -->
                <div class="block-area p-4 border bg-light-black">
                    <!-- Block title -->
                    <div class="block-title-6">
                        <h4 class="h5 border-primary">
                            <span class="bg-primary text-white">
								<?=$this->frontend_model->get_record("tbl_news_category", "id = 1", "name");?>
							</span>
                            <a href="<?=base_url()?>posts/1/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "id = 1", "name"))?>" class="pull-right">View all</a>
                        </h4>
                    </div>
                    <!-- block content -->
                    <div id="owl-carousel3" class="owl-carousel owl-theme">
                        
						<?php 
							foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '1' order by id desc") as $news):
							
							$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
							if($news->redirect_link != "")
							{
								$link = $news->redirect_link;
							}
						?>
							<!-- item slider -->
							<article class="item">
								<div class="col-12 mb-4">
									<div class="card card-full hover-a">
										<div class="ratio_327-278 image-wrapper">
											<!-- thumbnail -->
											<a href="<?=$link?>">
												<img class="img-fluid lazy news-img-class" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
											</a>
											<div class="position-absolute p-3 b-0 w-100 bg-shadow">
												<!-- post title -->
												<h4 class="h3 h4-sm h3-md card-title">
													<a class="text-white" href="<?=$link?>">
														<?=$news->title?>
													</a>
												</h4>
												<!-- date & rating -->
												<div class="small text-light">
													<time datetime="<?=$news->date?>"><?=date("d M, Y", strtotime($news->date))?></time>
												</div>
											</div>
										</div>
									</div>
								</div>
							</article>
							<!-- end item slider -->
						<?php endforeach; ?>
						
                    </div>
                    <!--end block content-->
                </div>
                <!-- End block -->
            </div>

            <?php include("news-left-navbar.php"); ?>
			
            <!--start left column-->
            <div class="col-md-6">
                <!-- Block start -->
                <div class="block-area">
                    <!--block title-->
                    <div class="block-title-6">
                        <h4 class="h5 border-primary">
                            <span class="bg-primary text-white">
								<?=$this->frontend_model->get_record("tbl_news_category", "id = 2", "name");?>
							</span>
                            <a href="<?=base_url()?>posts/2/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "id = 2", "name"))?>" class="pull-right">View all</a>
                        </h4>
                    </div>
                    <!--block content-->
                    <div class="row">
                        <!--post left start-->
                        <div class="col-lg-6">
							<?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '2' order by id desc limit 1") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<article class="card card-full hover-a mb-4">
									<!--thumbnail-->
									<div class="ratio_360-202 image-wrapper">
										<a href="<?=$link?>">
											<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
										</a>
									</div>
									<div class="card-body">
										<!-- title -->
										<h2 class="card-title h1-sm h3-lg">
											<a href="<?=$link?>">
												<?=$news->title?>
											</a>
										</h2>
										<!-- author, date and comments -->
										<div class="card-text mb-2 text-muted small">
											<time datetime="<?=$news->date?>">
												<?=date("d M, Y", strtotime($news->date))?>
											</time>
											<span title="9 likes" class="float-right mr--10">
												<span class="fa fa-thumbs-up" aria-hidden="true"></span>
												<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
											</span>
										</div>
										<!--description-->
										<p class="card-text">
											<?=$news->short_description?>
										</p>
									</div>
								</article>
							<?php endforeach; ?>
                        </div>
                        <!--end post left-->
                        <!--post right list start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '2' order by id desc limit 1,4") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<!--post list-->
								<article class="card card-full hover-a mb-4">
									<div class="row">
										<!--thumbnail-->
										<div class="col-3 col-md-4 pr-2 pr-md-0">
											<div class="ratio_115-80 image-wrapper">
												<a href="<?=$link?>">
													<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
												</a>
											</div>
										</div>
										<!-- title & date -->
										<div class="col-9 col-md-8">
											<div class="card-body pt-0">
												<h3 class="card-title h6 h5-sm h6-lg">
													<a href="<?=$link?>">
														<?=$news->title?>
													</a>
												</h3>
												<div class="card-text small text-muted">
													<time datetime="<?=$news->date?>">
														<?=date("d M, Y", strtotime($news->date))?>
													</time>
													<span title="9 likes" class="float-right mr--10">
														<span class="fa fa-thumbs-up" aria-hidden="true"></span>
														<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
													</span>
												</div>
											</div>
										</div>
									</div>
								</article>
								<!--End post list-->
							<?php endforeach; ?>
                        </div>
                        <!--end post right list-->
                    </div>
                    <!-- end block content -->
                </div>
                <!--End Block-->

                <!-- Block start -->
                <div class="block-area">
                    <!--block title-->
                    <div class="block-title-6">
                        <h4 class="h5 border-primary">
                            <span class="bg-primary text-white">
								<?=$this->frontend_model->get_record("tbl_news_category", "id = 3", "name");?>
							</span>
                            <a href="<?=base_url()?>posts/3/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "id = 3", "name"))?>" class="pull-right">View all</a>
                        </h4>
                    </div>
                    <!--block content-->
                    <div class="row">
                        <!--post left start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '3' order by id desc limit 1") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<article class="card card-full hover-a mb-4">
									<!--thumbnail-->
									<div class="ratio_360-202 image-wrapper">
										<a href="<?=$link?>">
											<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
										</a>
									</div>
									<div class="card-body">
										<!-- title -->
										<h2 class="card-title h1-sm h3-lg">
											<a href="<?=$link?>">
												<?=$news->title?>
											</a>
										</h2>
										<!-- author, date and comments -->
										<div class="card-text mb-2 text-muted small">
											<time datetime="<?=$news->date?>">
												<?=date("d M, Y", strtotime($news->date))?>
											</time>
											<span title="9 likes" class="float-right mr--10">
												<span class="fa fa-thumbs-up" aria-hidden="true"></span>
												<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
											</span>
										</div>
										<!--description-->
										<p class="card-text">
											<?=$news->short_description?>
										</p>
									</div>
								</article>
							<?php endforeach; ?>
                        </div>
                        <!--end post left-->
                        <!--post right list start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '3' order by id desc limit 1,4") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<!--post list-->
								<article class="card card-full hover-a mb-4">
									<div class="row">
										<!--thumbnail-->
										<div class="col-3 col-md-4 pr-2 pr-md-0">
											<div class="ratio_115-80 image-wrapper">
												<a href="<?=$link?>">
													<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
												</a>
											</div>
										</div>
										<!-- title & date -->
										<div class="col-9 col-md-8">
											<div class="card-body pt-0">
												<h3 class="card-title h6 h5-sm h6-lg">
													<a href="<?=$link?>">
														<?=$news->title?>
													</a>
												</h3>
												<div class="card-text small text-muted">
													<time datetime="<?=$news->date?>">
														<?=date("d M, Y", strtotime($news->date))?>
													</time>
													<span title="9 likes" class="float-right mr--10">
														<span class="fa fa-thumbs-up" aria-hidden="true"></span>
														<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
													</span>
												</div>
											</div>
										</div>
									</div>
								</article>
								<!--End post list-->
							<?php endforeach; ?>
                        </div>
                        <!--end post right list-->
                    </div>
                    <!-- end block content -->
                </div>
                <!--End Block-->

                <!-- Block start -->
                <div class="block-area">
                    <!--block title-->
                    <div class="block-title-6">
                        <h4 class="h5 border-primary">
                            <span class="bg-primary text-white">
								<?=$this->frontend_model->get_record("tbl_news_category", "id = 4", "name");?>
							</span>
                            <a href="<?=base_url()?>posts/4/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "id = 4", "name"))?>" class="pull-right">View all</a>
                        </h4>
                    </div>
                    <!--block content-->
                    <div class="row">
                        <!--post left start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '4' order by id desc limit 1") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<article class="card card-full hover-a mb-4">
									<!--thumbnail-->
									<div class="ratio_360-202 image-wrapper">
										<a href="<?=$link?>">
											<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
										</a>
									</div>
									<div class="card-body">
										<!-- title -->
										<h2 class="card-title h1-sm h3-lg">
											<a href="<?=$link?>">
												<?=$news->title?>
											</a>
										</h2>
										<!-- author, date and comments -->
										<div class="card-text mb-2 text-muted small">
											<time datetime="<?=$news->date?>">
												<?=date("d M, Y", strtotime($news->date))?>
											</time>
											<span title="9 likes" class="float-right mr--10">
												<span class="fa fa-thumbs-up" aria-hidden="true"></span>
												<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
											</span>
										</div>
										<!--description-->
										<p class="card-text">
											<?=$news->short_description?>
										</p>
									</div>
								</article>
							<?php endforeach; ?>
                        </div>
                        <!--end post left-->
                        <!--post right list start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '4' order by id desc limit 1,4") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<!--post list-->
								<article class="card card-full hover-a mb-4">
									<div class="row">
										<!--thumbnail-->
										<div class="col-3 col-md-4 pr-2 pr-md-0">
											<div class="ratio_115-80 image-wrapper">
												<a href="<?=$link?>">
													<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
												</a>
											</div>
										</div>
										<!-- title & date -->
										<div class="col-9 col-md-8">
											<div class="card-body pt-0">
												<h3 class="card-title h6 h5-sm h6-lg">
													<a href="<?=$link?>">
														<?=$news->title?>
													</a>
												</h3>
												<div class="card-text small text-muted">
													<time datetime="<?=$news->date?>">
														<?=date("d M, Y", strtotime($news->date))?>
													</time>
													<span title="9 likes" class="float-right mr--10">
														<span class="fa fa-thumbs-up" aria-hidden="true"></span>
														<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
													</span>
												</div>
											</div>
										</div>
									</div>
								</article>
								<!--End post list-->
							<?php endforeach; ?>
                        </div>
                        <!--end post right list-->
                    </div>
                    <!-- end block content -->
                </div>
                <!--End Block-->

                <!-- Block start -->
                <div class="block-area">
                    <!--block title-->
                    <div class="block-title-6">
                        <h4 class="h5 border-primary">
                            <span class="bg-primary text-white">
								<?=$this->frontend_model->get_record("tbl_news_category", "id = 5", "name");?>
							</span>
                            <a href="<?=base_url()?>posts/5/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "id = 5", "name"))?>" class="pull-right">View all</a>
                        </h4>
                    </div>
                    <!--block content-->
                    <div class="row">
                        <!--post left start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '5' order by id desc limit 1") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<article class="card card-full hover-a mb-4">
									<!--thumbnail-->
									<div class="ratio_360-202 image-wrapper">
										<a href="<?=$link?>">
											<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
										</a>
									</div>
									<div class="card-body">
										<!-- title -->
										<h2 class="card-title h1-sm h3-lg">
											<a href="<?=$link?>">
												<?=$news->title?>
											</a>
										</h2>
										<!-- author, date and comments -->
										<div class="card-text mb-2 text-muted small">
											<time datetime="<?=$news->date?>">
												<?=date("d M, Y", strtotime($news->date))?>
											</time>
											<span title="9 likes" class="float-right mr--10">
												<span class="fa fa-thumbs-up" aria-hidden="true"></span>
												<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
											</span>
										</div>
										<!--description-->
										<p class="card-text">
											<?=$news->short_description?>
										</p>
									</div>
								</article>
							<?php endforeach; ?>
                        </div>
                        <!--end post left-->
                        <!--post right list start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '5' order by id desc limit 1,4") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<!--post list-->
								<article class="card card-full hover-a mb-4">
									<div class="row">
										<!--thumbnail-->
										<div class="col-3 col-md-4 pr-2 pr-md-0">
											<div class="ratio_115-80 image-wrapper">
												<a href="<?=$link?>">
													<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
												</a>
											</div>
										</div>
										<!-- title & date -->
										<div class="col-9 col-md-8">
											<div class="card-body pt-0">
												<h3 class="card-title h6 h5-sm h6-lg">
													<a href="<?=$link?>">
														<?=$news->title?>
													</a>
												</h3>
												<div class="card-text small text-muted">
													<time datetime="<?=$news->date?>">
														<?=date("d M, Y", strtotime($news->date))?>
													</time>
													<span title="9 likes" class="float-right mr--10">
														<span class="fa fa-thumbs-up" aria-hidden="true"></span>
														<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
													</span>
												</div>
											</div>
										</div>
									</div>
								</article>
								<!--End post list-->
							<?php endforeach; ?>
                        </div>
                        <!--end post right list-->
                    </div>
                    <!-- end block content -->
                </div>
                <!--End Block-->

                <!-- Block start -->
                <div class="block-area">
                    <!--block title-->
                    <div class="block-title-6">
                        <h4 class="h5 border-primary">
                            <span class="bg-primary text-white">
								<?=$this->frontend_model->get_record("tbl_news_category", "id = 6", "name");?>
							</span>
                            <a href="<?=base_url()?>posts/6/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "id = 6", "name"))?>" class="pull-right">View all</a>
                        </h4>
                    </div>
                    <!--block content-->
                    <div class="row">
                        <!--post left start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '6' order by id desc limit 1") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<article class="card card-full hover-a mb-4">
									<!--thumbnail-->
									<div class="ratio_360-202 image-wrapper">
										<a href="<?=$link?>">
											<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
										</a>
									</div>
									<div class="card-body">
										<!-- title -->
										<h2 class="card-title h1-sm h3-lg">
											<a href="<?=$link?>">
												<?=$news->title?>
											</a>
										</h2>
										<!-- author, date and comments -->
										<div class="card-text mb-2 text-muted small">
											<time datetime="<?=$news->date?>">
												<?=date("d M, Y", strtotime($news->date))?>
											</time>
											<span title="9 likes" class="float-right mr--10">
												<span class="fa fa-thumbs-up" aria-hidden="true"></span>
												<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
											</span>
										</div>
										<!--description-->
										<p class="card-text">
											<?=$news->short_description?>
										</p>
									</div>
								</article>
							<?php endforeach; ?>
                        </div>
                        <!--end post left-->
                        <!--post right list start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '6' order by id desc limit 1,4") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<!--post list-->
								<article class="card card-full hover-a mb-4">
									<div class="row">
										<!--thumbnail-->
										<div class="col-3 col-md-4 pr-2 pr-md-0">
											<div class="ratio_115-80 image-wrapper">
												<a href="<?=$link?>">
													<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
												</a>
											</div>
										</div>
										<!-- title & date -->
										<div class="col-9 col-md-8">
											<div class="card-body pt-0">
												<h3 class="card-title h6 h5-sm h6-lg">
													<a href="<?=$link?>">
														<?=$news->title?>
													</a>
												</h3>
												<div class="card-text small text-muted">
													<time datetime="<?=$news->date?>">
														<?=date("d M, Y", strtotime($news->date))?>
													</time>
													<span title="9 likes" class="float-right mr--10">
														<span class="fa fa-thumbs-up" aria-hidden="true"></span>
														<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
													</span>
												</div>
											</div>
										</div>
									</div>
								</article>
								<!--End post list-->
							<?php endforeach; ?>
                        </div>
                        <!--end post right list-->
                    </div>
                    <!-- end block content -->
                </div>
                <!--End Block-->

                <!-- Block start -->
                <div class="block-area">
                    <!--block title-->
                    <div class="block-title-6">
                        <h4 class="h5 border-primary">
                            <span class="bg-primary text-white">
								<?=$this->frontend_model->get_record("tbl_news_category", "id = 7", "name");?>
							</span>
                            <a href="<?=base_url()?>posts/7/<?=$tis->slugify($this->frontend_model->get_record("tbl_news_category", "id = 7", "name"))?>" class="pull-right">View all</a>
                        </h4>
                    </div>
                    <!--block content-->
                    <div class="row">
                        <!--post left start-->
                        <div class="col-lg-6">
                            <?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '7' order by id desc limit 1") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<article class="card card-full hover-a mb-4">
									<!--thumbnail-->
									<div class="ratio_360-202 image-wrapper">
										<a href="<?=$link?>">
											<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
										</a>
									</div>
									<div class="card-body">
										<!-- title -->
										<h2 class="card-title h1-sm h3-lg">
											<a href="<?=$link?>">
												<?=$news->title?>
											</a>
										</h2>
										<!-- author, date and comments -->
										<div class="card-text mb-2 text-muted small">
											<time datetime="<?=$news->date?>">
												<?=date("d M, Y", strtotime($news->date))?>
											</time>
											<span title="9 likes" class="float-right mr--10">
												<span class="fa fa-thumbs-up" aria-hidden="true"></span>
												<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
											</span>
										</div>
										<!--description-->
										<p class="card-text">
											<?=$news->short_description?>
										</p>
									</div>
								</article>
							<?php endforeach; ?>
                        </div>
                        <!--end post left-->
                        <!--post right list start-->
                        <div class="col-lg-6">
							<?php 
								foreach($this->frontend_model->get_records("tbl_news", "status = '0' and category = '7' order by id desc limit 1,4") as $news):
								$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
								if($news->redirect_link != "")
								{
									$link = $news->redirect_link;
								}
							?>
								<!--post list-->
								<article class="card card-full hover-a mb-4">
									<div class="row">
										<!--thumbnail-->
										<div class="col-3 col-md-4 pr-2 pr-md-0">
											<div class="ratio_115-80 image-wrapper">
												<a href="<?=$link?>">
													<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="Spectra Sports">
												</a>
											</div>
										</div>
										<!-- title & date -->
										<div class="col-9 col-md-8">
											<div class="card-body pt-0">
												<h3 class="card-title h6 h5-sm h6-lg">
													<a href="<?=$link?>">
														<?=$news->title?>
													</a>
												</h3>
												<div class="card-text small text-muted">
													<time datetime="<?=$news->date?>">
														<?=date("d M, Y", strtotime($news->date))?>
													</time>
													<span title="9 likes" class="float-right mr--10">
														<span class="fa fa-thumbs-up" aria-hidden="true"></span>
														<?=sizeof($this->frontend_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $news->id . "'"))?>
													</span>
												</div>
											</div>
										</div>
									</div>
								</article>
								<!--End post list-->
							<?php endforeach; ?>
                        </div>
                        <!--end post right list-->
                    </div>
                    <!-- end block content -->
                </div>
                <!--End Block-->
            </div>
            <!--end left column-->

            <aside class="col-md-3 right-sidebar-lg">
				<?php include("new-right-sidebar.php"); ?>
			</aside>

        </div>
    </div>
</main>