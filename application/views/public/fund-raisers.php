<main id="main">

    <section id="why-us" class="why-us">
      <div class="container">
		<div class="row">
			<div class="col-xl-12 d-flex align-items-stretch">
				<div class="row">
					<?php $i = 1; ?>
					<?php foreach($records as $fr): ?>
				
						<div class="col-md-6">
							<div class="card fund-raiser-img-class">
								<a href="<?=base_url()?>fund-raiser/<?=$fr->id?>/<?=$tis->slugify($fr->purpose)?>">
									<img class="card-img-top" src="<?=base_url()?>uploads/service-requests/<?=$fr->raiser_image?>" alt="<?=base_url()?>uploads/service-requests/<?=$fr->raiser_image?>" />
								</a>
								<div class="card-name">
									<a href="<?=base_url()?>fund-raiser/<?=$fr->id?>/<?=$tis->slugify($fr->purpose)?>">
										<p>
											<?=$fr->purpose?>
										</p>
									</a>
								</div>
								<div class="card-location">
									<a href="<?=base_url()?>fund-raiser/<?=$fr->id?>/<?=$tis->slugify($fr->purpose)?>">
										<i class="fa fa-icon fa-map-marker"></i> 
										<?=$fr->city?>, <?=$fr->state?>
									</a>
								</div>
								<div class="card-sport">
									<a href="<?=base_url()?>fund-raiser/<?=$fr->id?>/<?=$tis->slugify($fr->purpose)?>">
										<?=$this->frontend_model->get_record("tbl_sports", "id=" . $fr->sport, "name")?>
									</a>
								</div>
								<div class="card-description">
									<?=html_entity_decode(substr($fr->described_purpose,"0","90"))?>...
								</div>
								
								<?php
									$percentage = ( $fr->raised_amount / $fr->fund_required ) * 100;
								?>
								
								<div class="card-progress">
									<div class="progress">
										<div class="progress-bar" style="width: <?=$percentage?>%;"></div>
									</div>
								</div>
								<div class="card-stats row">
									<div class="col-sm-4 text-center">
										₹<?=number_format($fr->raised_amount, 2)?> <br />
										<small>Raised</small>
									</div>
									<div class="col-sm-4 text-center">
										₹<?=number_format($fr->fund_required, 2)?> <br />
										<small>Target</small>
									</div>
									<div class="col-sm-4 text-center">
										<span class="daysleft">
											<?=sizeof($this->frontend_model->get_records("tbl_donors" ,"status = '0' and fund_raiser_id = '$fr->id' and is_paid = '1'"))?>
										</span> <br />
										<small>Supporters</small>
									</div>
								</div>
							</div>
						</div>
						<?php if($i%50 == 0 && $i != sizeof($records)): ?>
							</div>
							<div class="row"> 
						<?php endif; ?>
						<?php $i++; ?>
					<?php endforeach; ?>
					
					<?php if(sizeof($records) == 0): ?>
						<div class="col-md-12 text-center">
							No fund raisers found!
						</div>
					<?php endif; ?>
					
				</div>
			</div>
        </div>

      </div>
    </section>

</main>
