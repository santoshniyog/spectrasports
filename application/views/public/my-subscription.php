<?php
require_once(APPPATH.'config/database_config.php');
$id=$_SESSION['login_id'];
$query = "SELECT tbl_service_subscription.id,tbl_service_subscription.raised_amount,tbl_all_services.name,tbl_service_subscription.name_of_facility FROM tbl_service_subscription LEFT JOIN tbl_all_services ON tbl_service_subscription.service=tbl_all_services.id WHERE tbl_service_subscription.is_paid='1' AND tbl_service_subscription.user_id='$id'";
$query_run = mysqli_query($conn, $query);
?>
<style>
    .single-price {
        text-align: center;
        background: #262626;
        transition: .7s;
        margin-top: 20px;
    }

    .single-price h3 {
        font-size: 25px;
        color: #000;
        font-weight: 600;
        text-align: center;
        margin: 0;
        margin-top: -80px;
        font-family: poppins;
        color: #fff;
    }

    .single-price h4 {
        font-size: 48px;
        font-weight: 500;
        color: #fff;
    }

    .single-price h4 span.sup {
        vertical-align: text-top;
        font-size: 25px;
    }

    .deal-top {
        position: relative;
        background: #f22c4d;
        font-size: 16px;
        text-transform: uppercase;
        padding: 136px 24px 0;
    }

    .deal-top::after {
        content: "";
        position: absolute;
        left: 0;
        bottom: -50px;
        width: 0;
        height: 0;
        border-top: 50px solid #f22c4d;
        border-left: 127px solid transparent;
        border-right: 127px solid transparent;
    }

    .deal-bottom {
        padding: 56px 16px 0;
    }

    .deal-bottom ul {
        margin: 0;
        padding: 0;
    }

    .deal-bottom ul li {
        font-size: 16px;
        color: #fff;
        font-weight: 300;
        margin-top: 16px;
        border-top: 1px solid #E4E4E4;
        padding-top: 16px;
        list-style: none;
    }

    .btn-area a {
        display: inline-block;
        font-size: 18px;
        color: #fff;
        background: #f22c4d;
        padding: 8px 64px;
        margin-top: 32px;
        border-radius: 4px;
        margin-bottom: 40px;
        text-transform: uppercase;
        font-weight: bold;
        text-decoration: none;
    }


    .single-price:hover {
        background: #f22c4d;
    }

    .single-price:hover .deal-top {
        background: #262626;
    }

    .single-price:hover .deal-top:after {
        border-top: 50px solid #262626;
    }

    .single-price:hover .btn-area a {
        background: #262626;
    }


    a.view {
    color: #fff;
    background: #ee334e;
    padding: 4px 20px 4px 20px;
    font-size: 15px;
}


    /* ignore the code below */


    .link-area {
        position: fixed;
        bottom: 20px;
        left: 20px;
        padding: 15px;
        border-radius: 40px;
        background: tomato;
    }

    .link-area a {
        text-decoration: none;
        color: #fff;
        font-size: 25px;
    }
</style>

<!-- Contenedor -->


<!-- Titulo -->


<div class="pricing-area">

    <div class="container">
        <div class="row">
               
            <div class="col-md-12 col-sm-6 col-xs-12">
            <table class="table table-striped">
  <thead>
    <tr>
     
      <th scope="col">Subscription</th>
      <th scope="col">Amount</th>
      <th scope="col">Service Name</th>
      <th scope="col">Name of Facility</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <?php
  if (mysqli_num_rows($query_run) > 0) {
    while ($row = mysqli_fetch_assoc($query_run)) {
  ?>
    <tr>
    
    <td><?php echo $row['id']; ?></td>
    <td><?php echo $row['raised_amount']; ?></td>
    <td><?php echo $row['name']; ?></td>
    <td><?php echo $row['name_of_facility']; ?></td>
    <td>
      <a href="" class="view">Active</a>
    </td>
  </tr>
  <?php
    }}
  ?>
  
    
  </tbody>
</table>       
            </div>
         

        </div>
    </div>



</div>







<br>
<br>
<br>