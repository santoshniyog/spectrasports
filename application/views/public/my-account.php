
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<?php 
				//include("includes/profile-sidenav.php"); 
				?>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-6">
						<div class="mt--30 mb--30">
							<form class="update-my-profile">
								<div class="card">
									<?php if($this->session->flashdata('my-account-error-msg')): ?>
										<div class="card-header">
											<small>
												<b class="text-danger">Please complete this step to proceed next.</b>
											</small>
											<br>
											<small>
												<b class="text-danger">Please fill the mandatory details below.</b>
											</small>
										</div>
									<?php endif; ?>
									<div class="card-header">
										<h5>My Profile</h5>
									</div>
									<div class="card-body">
										<p class="form-row form-row-wide">
											<label for="first_name">First Name <span class="required">*</span></label>
											<input type="text" class="form-control required" value="<?=$user_details->first_name?>" name="first_name" id="first_name" value="" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="last_name">Last Name <span class="required">*</span></label>
											<input class="form-control required" type="text" value="<?=$user_details->last_name?>" name="last_name" id="last_name" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="last_name">Email <span class="required">*</span></label>
											<input class="form-control required" type="text" value="<?=$user_details->email?>" name="email" id="email" required="" autocomplete="off" disabled>
										</p>
										<p class="form-row form-row-wide">
											<label for="contact">Contact <span class="required">*</span></label>
											<input class="form-control" value="<?=$user_details->phone_number?>" type="text" readonly="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="contact">Short Bio <span class="required">*</span></label>
											<input class="form-control required" value="<?=$user_details->short_bio?>" name="short_bio" type="text" autocomplete="off" placeholder="Eg: Athlete in 400m Sprinter" required> 
										</p>
										<?php if($this->frontend_model->get_record("tbl_services", "id=" . $user_details->service, "name") == "Academy" || $this->frontend_model->get_record("tbl_services", "id=" . $user_details->service, "name") == "University"): ?>
											<p class="form-row form-row-wide">
												<label for="contact">Organization Name <span class="required">*</span></label>
												<input class="form-control" value="<?=$user_details->organization_name?>" type="text" readonly>
											</p>
										<?php endif; ?>
										<p class="form-row form-row-wide">
											<label for="service">Choose Role <span class="required">*</span></label>
											<select class="form-control" readonly>	
												<?php foreach($this->frontend_model->get_records('tbl_services', "status = '0'") as $service): ?>
												<option <?=($user_details->service==$service->id)?"selected":""?> value="<?=$service->id?>"><?=$service->name?></option>
												<?php endforeach; ?>
											</select>
										</p>
										<p class="form-row form-row-wide">
											<label for="interested_sport">Interested Sport <span class="required">*</span></label>
											<select class="form-control" name="interested_sport" id="interested_sport" required>	
												<?php foreach($this->frontend_model->get_records('tbl_sports', "status = '0'") as $sport): ?>
												<option <?=($user_details->interested_sport==$sport->name)?"selected":""?> value="<?=$sport->name?>"><?=$sport->name?></option>
												<?php endforeach; ?>
											</select>
										</p>	
										<p class="form-row form-row-wide">
											<label for="date_of_birth">Date of birth <span class="required">*</span></label>
											<input class="form-control" type="date" value="<?=$user_details->date_of_birth?>" name="date_of_birth" id="date_of_birth" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="gender">Gender <span class="required">*</span></label>
											<select class="form-control" name="gender" id="gender" required="" autocomplete="off">
												<option <?=($user_details->gender == "male")?"selected":""?> value="male">Male</option>
												<option <?=($user_details->gender == "female")?"selected":""?> value="female">Female</option>
											</select>
										</p>
										<p class="form-row form-row-wide">
											<label for="address">Address<span class="required">*</span></label>
											<input class="form-control" type="text" value="<?=$user_details->address?>" name="address" id="address" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="city">City <span class="required">*</span></label>
											<input class="form-control" type="text" value="<?=$user_details->city?>" name="city" id="city" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="state">State <span class="required">*</span></label>
											<input class="form-control" type="text" value="<?=$user_details->state?>" name="state" id="state" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="pincode">Pincode <span class="required">*</span></label>
											<input class="form-control" type="text" value="<?=$user_details->pincode?>" name="pincode" id="pincode" required="" autocomplete="off">
										</p>
									</div>
									<div class="card-footer">
										<p class="form-row form-row-wide">
											<button type="submit" class="button pull-right">Update</button>
										</p>
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="mt--30 mb--30">
							<form class="my-profile-change-password">
								<div class="card">
									<div class="card-header">
										<h5>Change Password</h5>
									</div>
									<div class="card-body">	
										<p class="form-row form-row-wide">
											<label for="currentpassword">Current Password <span class="required">*</span></label>
											<input class="form-control" type="password" name="currentpassword" id="currentpassword" required="" autocomplete="off">
										</p>	
										<p class="form-row form-row-wide">
											<label for="password">New Password <span class="required">*</span></label>
											<input class="form-control" type="password" name="password" id="password" required="" autocomplete="off">
										</p>	
										<p class="form-row form-row-wide">
											<label for="confirm_password">Confirm New Password <span class="required">*</span></label>
											<input class="form-control" type="password" name="confirm_password" id="confirm_password" required="" autocomplete="off">
										</p>	
									</div>
									<div class="card-footer">
										<p class="form-row form-row-wide">
											<button type="submit" class="button pull-right">Submit</button>
										</p>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>