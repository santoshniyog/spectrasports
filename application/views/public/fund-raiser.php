<main id="main">
    <section id="why-us" class="why-us">
      <div class="container">

        

		<div class="row">
			<div class="col-xl-4">
				<div class="card">
					<div class="card-body">
						<div class="card-name">
							<a href="<?=base_url()?>fund-raiser/<?=$record->id?>/<?=$tis->slugify($record->purpose)?>">
								<p>
									<?=$record->purpose?>
								</p>
							</a>
						</div>
						<div class="card-location">
							<a href="<?=base_url()?>fund-raiser/<?=$record->id?>/<?=$tis->slugify($record->purpose)?>">
								<i class="fa fa-icon fa-map-marker"></i> 
								<?=$record->city?>, <?=$record->state?>
							</a>
						</div>
						<div class="card-sport">
							<a href="<?=base_url()?>fund-raiser/<?=$record->id?>/<?=$tis->slugify($record->purpose)?>">
								<?=$this->frontend_model->get_record("tbl_sports", "id=" . $record->sport, "name")?>
							</a>
						</div>						
						<?php
							$percentage = ( $record->raised_amount / $record->fund_required ) * 100;
						?>
						
						<div class="card-progress">
							<div class="progress">
								<div class="progress-bar" style="width: <?=$percentage?>%;"></div>
							</div>
						</div>
						<div class="card-stats row">
							<div class="col-sm-4 text-center">
								₹<?=number_format($record->raised_amount, 2)?> <br />
								<small>Raised</small>
							</div>
							<div class="col-sm-4 text-center">
								₹<?=number_format($record->fund_required, 2)?> <br />
								<small>Target</small>
							</div>
							<div class="col-sm-4 text-center">
								<span class="daysleft">
									<?=sizeof($this->frontend_model->get_records("tbl_donors" ,"status = '0' and fund_raiser_id = '$record->id' and is_paid = '1'"))?>
								</span> <br />
								<small>Supporters</small>
							</div>
						</div>
						<div class="card-name text-center">
							<a href="javascript: void(0);" class="btn btn-danger text-white" data-target="#donate-now" data-toggle="modal">
								Donate Now
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-8 align-items-stretch">
			<div class="banner_img">
			<img class="w-100" src="<?=base_url()?>uploads/service-requests/<?=$record->raiser_image?>">
			</div>
				<div class="price">
					<div class="card-body">
						<?=html_entity_decode($record->described_purpose)?> 
					</div>
				</div>
			</div>
        </div>

      </div>
    </section>
</main>


	
<div id="donate-now" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-body">
		<button data-dismiss="modal" class="close">×</button>
		<div class="col2-set" id="customer_login2">
			<div class="col-lg-12">
				<div class="form-design">
					<h4>Donate Now</h4>
					<hr>
					<form method="post" class="register form-donate-now">
						<div class="row">
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="name">Name <span class="required">*</span></label>
									<input type="text" class="form-control" name="name" id="name" required value="<?=(isset($user_details))?$user_details->first_name:""?> <?=(isset($user_details))?$user_details->last_name:""?>"/>
									<input type="hidden" name="fund_raiser_id" value="<?=$record->id?>"/> 
								</p>
							</div>
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="amount">Amount <span class="required">*</span></label>
									<input type="number" value="100" min="100" class="form-control" name="amount" id="amount" required/>
								</p>
							</div>
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="city">City <span class="required">*</span></label>
									<input type="text" class="form-control" name="city" id="city" required value="<?=(isset($user_details))?$user_details->city:""?>"/>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="phone_number">Mobile Number</label>
									<input type="number" class="form-control" name="phone_number" id="phone_number" value="<?=(isset($user_details))?$user_details->phone_number:""?>"/>
								</p>
							</div>
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="email">Email</label>
									<input type="email" class="form-control" name="email" id="email"/>
								</p>
							</div>
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="address">Address</label>
									<input type="text" class="form-control" name="address" id="address" value="<?=(isset($user_details))?$user_details->address:""?>"/>
								</p>
							</div>
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="state">State</label>
									<input type="text" class="form-control" name="state" id="state" value="<?=(isset($user_details))?$user_details->state:""?>"/>
								</p>
							</div>
							<div class="col-md-6">
								<p class="form-row form-row-wide">
									<label for="pincode">Pincode</label>
									<input type="text" class="form-control" name="pincode" id="pincode" value="<?=(isset($user_details))?$user_details->pincode:""?>"/>
								</p>
							</div>
							<div class="col-md-12">
								<p class="form-row form-row-wide">
									<label for="terms">
										<input type="checkbox" id="terms" required name="terms" class="input-checkbox">
										I’ve read and accept the 
										<a target="_blank" href="<?=base_url()?>terms-conditions" class="text-underline">terms &amp; conditions</a> 
										<span class="required">*</span>
									</label>
								</p>
							</div>
							<div class="col-md-12">
								<p class="form-row form-row-wide">
									<button type="submit" class="button pull-right btn btn-danger">Donate</button>
								</p>
							</div>
						</div>
					</form>	
					<hr>
					<small><a href="<?=base_url()?>login">Have an account? Login here.</a></small><br>
							
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>  
</div>

<script src="https://js.instamojo.com/v1/checkout.js"></script>
<script>
	function payment(url) {
		Instamojo.open(url);
	}
</script> 
