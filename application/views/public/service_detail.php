
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="mt--30 mb--30 align-items-stretch">
					<div class="bg-white box-shadow mt-4 mt-xl-0">
						<div class="card-box pb--0">
							<img src="<?=base_url()?>uploads/<?=$service->image?>" width="75">
							<br>
							<br>
							<h4><?=$service->name?></h4>
						</div>
						<div class="card-box pt--0 min-height-150px">
							<p><?=$service->short_description?></p>
						</div>
						<div class="card-box pt--0">
							<a class="card-link btn btn-sm btn-secondary" href="<?=base_url()?>services">All Services</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="mt--30 mb--30">
					<div class="card">
						<div class="card-header">
							<h5><?=$service->name?></h5>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<?=$service->description?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>