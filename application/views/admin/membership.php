<?php
require_once(APPPATH.'config/database_config.php');
$query = "SELECT * FROM tbl_membership";
$query_run = mysqli_query($conn, $query);
?>
<style>
  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }

  .switch input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }
</style>
<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body table-responsive">
            <table class="table table-hover data_table">
              <thead>
                <tr>
                  <th>Sl. No.</th>
                  <th>Type</th>
                  <th>price</th>
                  <th>Line1</th>
                  <th>line2</th>
                  <th>line3</th>
                  <th>line4</th>
                  <th>line5</th>
                  <th class="text-center">Edit</th>
                  <th class="text-center">Delete</th>
                </tr>
              </thead>
              <tbody>

                <?php

                if (mysqli_num_rows($query_run) > 0) {
                  while ($row = mysqli_fetch_assoc($query_run)) {
                    $id = $row['id'];
                ?>

                    <tr>
                      <td><?php echo $row['id']; ?> </td>
                      <td><?php echo $row['type']; ?> </td>
                      <td><?php echo $row['price']; ?> </td>
                      <td><?php echo $row['line1']; ?> </td>
                      <td><?php echo $row['line2']; ?> </td>
                      <td><?php echo $row['line3']; ?> </td>
                      <td><?php echo $row['line4']; ?> </td>
                      <td><?php echo $row['line5']; ?> </td>

                      <td>
                        <input type="hidden" name="edit_id" value="<?php echo $row['id']; ?>">
                        <a class="btn btn-sm btn-warning" href="<?= base_url() ?>admin/edit-membership/<?php echo $id; ?>">Edit</a>

                      </td>
                      <td>
                        <form action="" method="post" reload-action="true">
                          <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                          <?php if ($row['status'] == '1') {
                          ?> <input type="submit" value="Disable" name="disable" class="btn btn-sm btn btn-danger"></input>
                          <?php } else {
                          ?> <input type="submit" value="Enable" name="enable" class="btn btn-sm btn btn-primary"> </input>
                          <?php }
                          ?>

                        </form>
                        <!-- <form class="update_data update_data_<?php echo $row['id']; ?>" this_id="form-<?= uniqid() ?>" reload-action="true">
                          <input type="hidden" name="table_name" value="tbl_news">
                          <input type="hidden" name="row_id" value="<?php echo $row['id']; ?>">
                          <input type="hidden" name="status" value="1">
                          <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                        </form> -->
                      </td>


                    </tr>
                  <?php
                  }
                } else {
                  echo "no record found";
                }

                if (isset($_POST['disable'])) {

                  $id = $_POST['id'];

                  $query1 = "UPDATE tbl_membership SET  status='0' WHERE id='$id'";
                  $query_run = mysqli_query($conn, $query1);

                  if ($query_run) {
                    header("Refresh:0");
                  ?>

                    <div class="alert alert-success">
                      <strong>Update Success!</strong> .
                    </div>

                  <?php
                  } else {
                  ?>
                    <div class="alert alert-warning">
                      <strong>Warning!</strong> Not Update..
                    </div>
                  <?php

                  }
                }

                if (isset($_POST['enable'])) {

                  $id = $_POST['id'];

                  $query2 = "UPDATE tbl_membership SET  status='1' WHERE id='$id'";
                  $query_run = mysqli_query($conn, $query2);

                  if ($query_run) {
                    header("Refresh:0");
                  ?>

                    <div class="alert alert-success">
                      <strong>Update Success!</strong> .
                    </div>

                  <?php
                  } else {

                  ?>
                    <div class="alert alert-warning">
                      <strong>Warning!</strong> Not Update..
                    </div>
                  <?php

                  }
                }
                if (isset($_POST['delete_btn'])) {
                  $id = $_POST['id'];

                  $query1 = "DELETE FROM tbl_feedback WHERE id='$id'";
                  $query1_run = mysqli_query($conn, $query1);

                  if ($query_run) {
                    header("Refresh:0");

                  ?>
                    <div class="alert alert-warning">
                      <strong>Your Data Deleted!</strong> sucessfully..
                    </div>

                  <?php
                  } else {
                  ?>
                    <div class="alert alert-warning">
                      <strong>Your Data not Deleted!</strong> sucessfully..
                    </div>
                <?php
                  }
                }


                ?>


              </tbody>
            </table>

          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
  </section>
</div>



<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>