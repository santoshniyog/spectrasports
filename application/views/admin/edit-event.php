<div class="content-wrapper">
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">
							Edit Event <small>(All the fields are mandatory.)</small>
						</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" class="update_data" this_id="form-001" action="#" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>User</label>
                                        <input type="text" value="<?=ucfirst($this->common_model->get_record("tbl_general_users", "id = '$record->user_id'", "first_name")) . " " . ucfirst($this->common_model->get_record("tbl_general_users", "id = '$record->user_id'", "last_name"))?>" class="form-control" readonly>
										<input type="hidden" name="table_name" value="tbl_events">
										<input type="hidden" name="row_id" value="<?=$record->id?>">
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="event_name">Event Name</label>
                                        <input type="text" value="<?=$record->event_name?>" class="form-control required" name="event_name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">Choose Sport</label>
                                        <select class="form-control" name="sport" id="sport">	
											<?php foreach($this->common_model->get_records('tbl_sports', "status = '0'") as $sport): ?>
												<option <?=($record->sport == $sport->id)?"selected":""?> value="<?=$sport->id?>"><?=$sport->name?></option>
											<?php endforeach; ?>
										</select>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="booking_link">Booking Link</label>
                                        <input type="text" value="<?=$record->booking_link?>" class="form-control required" name="booking_link" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="text">Price</label>
                                        <input type="text" value="<?=$record->price?>" class="form-control required" name="price" required>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">price for</label>
                                        <select class="form-control" name="price_for" id="price_for">
											<option <?=($record->price_for == "per ticket")?"selected":""?>>per ticket</option>
											<option <?=($record->price_for == "per team")?"selected":""?>>per team</option>
											<option <?=($record->price_for == "onwards")?"selected":""?>>onwards</option>
										</select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">Short Description</label>
                                        <textarea class="form-control required" type="text" name="short_description" id="short_description" required="" autocomplete="off"><?=$record->short_description?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">Event Date</label>
                                        <input type="date" class="form-control required" name="event_date" id="event_date" required="" autocomplete="off" value="<?=$record->event_date?>">
                                    </div>
                                </div>
							</div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">Location / Venue</label>
                                        <input type="text" class="form-control required" name="location" id="location" required="" autocomplete="off" value="<?=$record->location?>">
                                    </div>
                                </div>
							</div>
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control required" name="description" id="editor" type="text" required="" autocomplete="off"><?=$record->description?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">                                
                                    <a href="<?=base_url()?>uploads/<?=$record->event_image?>" target="_blank">
										<img src="<?=base_url()?>uploads/<?=$record->event_image?>" width="200" alt="<?=base_url()?>uploads/<?=$record->event_image?>">
									</a>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">Event Poster</label>
                                        <input type="file" class="form-control" name="event_image" id="event_image">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/dist/js/ckeditor.js" charset="utf-8"></script>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>