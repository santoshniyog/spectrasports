<?php
require_once(APPPATH.'config/database_config.php');
$query = "SELECT * FROM tbl_feedback";
$query_run = mysqli_query($conn, $query);
?>

<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body table-responsive">
            <table class="table table-hover data_table">
              <thead>
                <tr>
                  <th>Sl. No.</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Short Description</th>
                  <th>Description</th>
                  <th class="text-center">Edit</th>
                  <th class="text-center">Delete</th>
                </tr>
              </thead>
              <tbody>

                <?php

                if (mysqli_num_rows($query_run) > 0) {
                  while ($row = mysqli_fetch_assoc($query_run)) {
                    $id = $row['id'];
                ?>

                    <tr>
                      <td><?php echo $row['id']; ?> </td>
                      <td><img src=" <?php echo base_url() . $row['image_url']; ?>" width="150"> </td>
                      <td><?php echo $row['title']; ?> </td>
                      <td><?php echo $row['short_description']; ?> </td>
                      <td><?php echo $row['description']; ?> </td>
                      <td>
                        <input type="hidden" name="edit_id" value="<?php echo $row['id']; ?>">
                        <a class="btn btn-sm btn-warning" href="<?= base_url() ?>admin/edit-feedback/<?php echo $id; ?>">Edit</a>

                      </td>
                      <td>
                        <form action="" method="post"  reload-action="true">
                          <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                          <button type="submit" name="delete_btn" class="btn btn-danger">DELECT</button>
                        </form>
                        <!-- <form class="update_data update_data_<?php echo $row['id']; ?>" this_id="form-<?= uniqid() ?>" reload-action="true">
                          <input type="hidden" name="table_name" value="tbl_news">
                          <input type="hidden" name="row_id" value="<?php echo $row['id']; ?>">
                          <input type="hidden" name="status" value="1">
                          <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                        </form> -->
                      </td>


                    </tr>
                  <?php
                  }
                } else {
                  echo "no record found";
                }

                 
                if (isset($_POST['delete_btn'])) {
                  $id = $_POST['id'];

                  $query1 = "DELETE FROM tbl_feedback WHERE id='$id'";
                  $query1_run = mysqli_query($conn, $query1);

                  if ($query_run) {
                    header("Refresh:0");

                  ?>
                    <div class="alert alert-warning">
                      <strong>Your Data Deleted!</strong> sucessfully..
                    </div>
                    
                  <?php
                  } else {
                  ?>
                    <div class="alert alert-warning">
                      <strong>Your Data not Deleted!</strong> sucessfully..
                    </div>
                <?php
                  }
                   
                }


                ?>


              </tbody>
            </table>

          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
  </section>
</div>



<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>