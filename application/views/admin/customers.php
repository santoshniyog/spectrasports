<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body table-responsive">
                  <table class="table table-hover data_table">
					<thead>
						<tr>
							<th>Sl. No.</th>
							<th>Name</th>
							<th>Mobile Number</th>
							<th>Created at</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($records))
						{
							$inc = 1;
							foreach($records as $record)
							{
						?>
						<tr>
							<td><?php echo $inc; ?></td>
							<td>
								<?=ucfirst($this->common_model->get_record('tbl_general_users', "status = '0' and id=" . $record->id, 'first_name')) ?> 
								<?=ucfirst($this->common_model->get_record('tbl_general_users', "status = '0' and id=" . $record->id, 'last_name')) ?>
							</td>
							<td><?php echo $record->phone_number ?></td>
							<td><?php echo date("D d, M Y", strtotime($record->date_time))?></td>
							<td class="text-center">
								<a class="btn btn-sm btn-info" data-toggle="modal" data-target="#editSlider" onclick="get_records('tbl_category_images', 'brand_id=<?=$record->id?>')"><i class="fa fa-book"></i></a>
								<a class="btn btn-sm btn-danger deletebtn" style="<?php if($record->status == 1){ echo 'display: none;'; } ?>" href="#" data-table="tbl_brands" data-id="<?php echo $record->id; ?>"><i class="fa fa-close"></i></a>
							</td>
						</tr>
						<?php
							$inc++;
							}
						}
						?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
