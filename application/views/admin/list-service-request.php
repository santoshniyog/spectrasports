<style>
table.dataTable tbody th, table.dataTable tbody td {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 13ch;
}
</style>
<div class="content-wrapper">
    
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body table-responsive">
                  <table class="table table-hover data_table">
					<thead>
						<tr>
							<th></th>
							<th></th>
							<th>Sl. No.</th>
							<th>Registered Date</th>
							<th>User</th>
							<th>Name</th>
							<th>Date of birth</th>
							<th>Gender</th>
							<th>Sport</th>
							<th>Address</th>
							<th>City</th>
							<th>State</th>
							<th>Pincode</th>
							<th>Designation</th>
							<th>Name of facility</th>
							<?php if($service_id == '1'): ?>
								<th>Level of participation</th>
							<?php endif; ?>
							<?php if($service_id == '4'): ?>
								<th>Purpose in short</th>
								<th>Fund Raised</th>
								<th>Fund required</th>
								<th>Describe purpose</th>
							<?php endif; ?>
							<?php if($service_id == '3'): ?>
								<th>Career option</th>
							<?php endif; ?>
							<th>Specific Requirement</th>
							<th>Uploaded Proof</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($records))
						{
							$inc = 1;
							foreach($records as $record)
							{
						?>
						<tr>
							<td>
								<a href="<?=base_url()?>admin/view-service-request/<?=$record->id; ?>" class="btn btn-sm btn-primary">View</a>
							</td>
							<td>
								<a href="<?=base_url()?>admin/edit-service-request/<?=$record->id; ?>" class="btn btn-sm btn-warning">Edit</a>
							</td>
							<td><?php echo $inc; ?></td>
							<td><?php echo date("d M, Y", strtotime($record->date_time))?></td>
							<td>
								<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "first_name")); ?>
								<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "last_name")); ?>
								<br>
								<b>(<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "phone_number")); ?>)</b>
								<br>
								<?php $ser = $this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "service"); ?>
								<b>(<?=$this->common_model->get_record("tbl_services", "id=" . $ser, "name"); ?>)</b>
							</td>
							<td><?php echo $record->name?></td>
							<td><?php echo $record->date_of_birth ?></td>
							<td><?php echo $record->gender ?></td>
							<td><?php echo $this->common_model->get_record("tbl_sports", "id=" . $record->sport, "name"); ?></td>
							<td><?php echo $record->address ?></td>
							<td><?php echo $record->city ?></td>
							<td><?php echo $record->state ?></td>
							<td><?php echo $record->pincode ?></td>
							<td><?php echo $record->designation ?></td>
							<td><?php echo $record->name_of_facility ?></td>
							<?php if($service_id == '1'): ?>
								<td><?php echo $record->level_of_participation ?></td>
							<?php endif; ?>
							<?php if($service_id == '4'): ?>
								<td><?php echo $record->purpose ?></td>
								<td><?php echo $record->raised_amount?></td>
								<td><?php echo $record->fund_required ?></td>
								<td><?php echo $record->described_purpose ?></td>
							<?php endif; ?>
							<?php if($service_id == '3'): ?>
								<td><?php echo $record->career_option ?></td>
							<?php endif; ?>
							<td><?php echo $record->specific_requirements ?></td>
							<td>
								<a target="_blank" href="<?=base_url()?>uploads/service-requests/<?php echo $record->proof_upload ?>">Download</a>
							</td>
							<td>
								<a href="<?=base_url()?>admin/view-service-request/<?=$record->id; ?>" class="btn btn-sm btn-primary">View</a>
							</td>
							<td>
								<a href="<?=base_url()?>admin/edit-service-request/<?=$record->id; ?>" class="btn btn-sm btn-warning">Edit</a>
							</td>
						</tr>
						<?php
							$inc++;
							}
						}
						?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
