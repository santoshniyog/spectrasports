<?php
require_once(APPPATH.'config/database_config.php');
?>
<style>
.label
{
    background-color: black;
}

</style>
<div class="content-wrapper">
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Edit Membership Plan<small>(All the fields are mandatory.)</small></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form  action=""     method="POST"  enctype="multipart/form-data">
                        <div class="box-body">
                              
                               

                              <input type="hidden" name="id" value="<?=$record->id?>">
                              <!-- <input type="hidden" name="image_old" value="<?=$record->image_url?>"> -->
                              
                                      
                               <div class="row">
                                <!-- <div class="col-md-4">                                
                                    <a href="<?=base_url()?><?=$record->image_url?>" target="_blank">
										<img src="<?=base_url()?><?=$record->image_url?>" width="100" alt="<?=base_url()?><?=$record->image_url?>">
									</a>
                                </div> -->
                                <!-- <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">Event Poster</label>
                                        <input type="file"  class="form-control" name="image" id="event_image">
                                    </div>
                                </div> -->
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="type"> Type </label>
                                        <input type="text"   value="<?=$record-> type?>" class="form-control required" name="type">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text"   value="<?=$record->price?>" class="form-control required" name="price">
                                    </div>
                                </div>
                            </div>
                                                       
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="description">Line1</label>
                                        <input type="text"  class="form-control" value="<?=$record->line1?>" name="line1"></input>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="type">Line2</label>
                                        <input type="text"   value="<?=$record-> line2?>" class="form-control required" name="line2">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="price">Line3</label>
                                        <input type="text"  value="<?=$record->line3?>" class="form-control required" name="line3">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="description">Line4</label>
                                        <input type="text" class="form-control" value="<?=$record->line4?>" name="line4"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="description">Line5</label>
                                        <input type="text" class="form-control" value="<?=$record->line5?>" name="line5"></input>
                                    </div>
                                </div>
                            </div>
                    
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Update" name="updatebtn"/>
                            <!-- <input type="reset" class="btn btn-default" value="Reset"/> -->
                        </div>
                    </form>
                    <?php
                    //print_r($_POST);
                     if(isset($_POST['updatebtn']))
                     {
                         
                         
                         $type = $_POST['type'];
                         $price = $_POST['price'];
                         $line1 = $_POST['line1'];
                         $line2 = $_POST['line2'];
                         $line3 = $_POST['line3'];
                         $line4 = $_POST['line4'];
                         $line5 = $_POST['line5'];
                         $id=$_POST['id'];
                       
                       $query= "UPDATE tbl_membership SET  type='$type',price='$price',line1='$line1',line2='$line2',line3='$line3',line4='$line4',line5='$line5' WHERE id='$id'";
                       $query_run = mysqli_query($conn,$query);
                       print_r($query);
                       if($query_run)
                       {
                             header("Refresh:0");
                            ?>

                           <div class="alert alert-success">
                           <strong>Update Success!</strong> .
                         </div>

                          <?php
                       }

                       else
                       {
                           ?>
                        <div class="alert alert-warning">
                        <strong>Warning!</strong> Not Update..
                      </div> 
                      <?php

                       }
                     }
                     ?>
                   
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>