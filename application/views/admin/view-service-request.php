<?php $service_id = $records[0]->service; ?>
<div class="content-wrapper">
    
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body table-responsive">
                  <table class="table table-hover">
					<tbody>
						<?php
						if(!empty($records))
						{
							$inc = 1;
							foreach($records as $record)
							{
						?>
							<tr>
								<th>Sl. No.</th>
								<td><?php echo $inc; ?></td>
							</tr>
							<tr>
								<th>Registered Date</th>
								<td><?php echo date("d M, Y", strtotime($record->date_time))?></td>
							</tr>
							<tr>
								<th>User</th>
								<td>
									<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "first_name")); ?>
									<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "last_name")); ?>
									<br>
									<b>(<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "phone_number")); ?>)</b>
									<br>
									<?php $ser = $this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "service"); ?>
									<b>(<?=$this->common_model->get_record("tbl_services", "id=" . $ser, "name"); ?>)</b>
								</td>
							</tr>
							<tr>
								<th>Name</th>
								<td><?php echo $record->name?></td>
							</tr>
							<tr>
								<th>Date of birth</th>
								<td><?php echo $record->date_of_birth ?></td>
							</tr>
							<tr>
								<th>Gender</th>
								<td><?php echo $record->gender ?></td>
							</tr>
							<tr>
								<th>Sport</th>
								<td><?php echo $this->common_model->get_record("tbl_sports", "id=" . $record->sport, "name"); ?></td>
							</tr>
							<tr>
								<th>Address</th>
								<td><?php echo $record->address ?></td>
							</tr>
							<tr>
								<th>City</th>
								<td><?php echo $record->city ?></td>
							</tr>
							<tr>
								<th>State</th>
								<td><?php echo $record->state ?></td>
							</tr>
							<tr>
								<th>Pincode</th>
								<td><?php echo $record->pincode ?></td>
							</tr>
							<tr>
								<th>Designation</th>
								<td><?php echo $record->designation ?></td>
							</tr>
							<tr>
								<th>Name of facility</th>
								<td><?php echo $record->name_of_facility ?></td>
							</tr>
							<tr>
							<?php if($service_id == '1'): ?>
									<th>Level of participation</th>
									<td><?php echo $record->level_of_participation ?></td>
								</tr>
								<tr>
							<?php endif; ?>
							<?php if($service_id == '4'): ?>
									<th>Purpose in short</th>
									<td><?php echo $record->purpose ?></td>
								</tr>
								<tr>
									<th>Fund Raised</th>
									<td>
										<?php echo $record->raised_amount?>
									</td>
								</tr>
								<tr>
									<th>Fund required</th>
									<td><?php echo $record->fund_required ?></td>
								</tr>
								<tr>
									<th>Described Purpose</th>
									<td><?php echo $record->described_purpose ?></td>
								</tr>
								<tr>
							<?php endif; ?>
							<?php if($service_id == '3'): ?>
									<th>Career option</th>
									<td><?php echo $record->career_option ?></td>
								</tr>
								<tr>
							<?php endif; ?>
								<th>Specific Requirement</th>
								<td><?php echo $record->specific_requirements ?></td>
							</tr>
							<tr>
								<th>Proof</th>
								<td>
									<a target="_blank" href="<?=base_url()?>uploads/service-requests/<?php echo $record->proof_upload ?>">Download</a>
								</td>
							</tr>
						<?php
							$inc++;
							}
						}
						?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
