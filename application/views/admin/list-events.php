<div class="content-wrapper">
    
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body table-responsive">
                  <table class="table table-hover data_table">
					<thead>
						<tr>
							<th>Sl. No.</th>
							<th>Poster</th>
							<th>Name</th>
							<th>Sport</th>
							<th>Registered Date</th>
							<th class="text-center">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($records))
						{
							$inc = 1;
							foreach($records as $record)
							{
						?>
						<tr>
							<td><?php echo $inc; ?></td>
							<td>
								<a href="<?=base_url()?>uploads/<?=$record->event_image?>" target="_blank">
									<img width="50" src="<?=base_url()?>uploads/<?=$record->event_image?>">
								</a>
							</td>
							<td>
								<a href="<?=base_url()?>event/<?=$tis->slugify($record->event_name);?>/<?=base64_encode($record->id)?>" target="_blank">
									<?php echo $record->event_name ?>
								</a>
							</td>
							<td><?php echo $this->common_model->get_record("tbl_sports", "id=" . $record->sport, "name"); ?></td>
							<td><?php echo $record->date_time; ?></td>
							<td>
								<a class="btn btn-sm btn-warning" href="<?=base_url()?>admin/edit-event/<?=$record->id?>">Edit</a>
							</td>
							<td class="text-center">
								<form class="update_data update_data_<?=$record->id?>" this_id="form-<?=uniqid()?>">
									<select class="form-control required" name="event_status" onchange="$('.update_data_<?=$record->id?>').submit();">
										<option <?=($record->event_status=="pending"?"selected":"")?> value="pending">Pending</option>
										<option <?=($record->event_status=="approved"?"selected":"")?> value="approved">Approve</option>
										<option <?=($record->event_status=="declined"?"selected":"")?> value="declined">Decline</option>
									</select>
									<input type="hidden" name="table_name" value="tbl_events" class="required">
									<input type="hidden" name="row_id" value="<?=$record->id?>">
								</form>
							</td>
						</tr>
						<?php
							$inc++;
							}
						}
						?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
		
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
