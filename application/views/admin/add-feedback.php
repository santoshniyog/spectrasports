<?php
require_once(APPPATH.'config/database_config.php');

?>

<div class="content-wrapper">
    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Feedback <small>(All the fields are mandatory.)</small></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="box-body">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control required" name="image">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control required" name="title">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="short_description">Short Description</label>
                                    <input type="text" class="form-control required" name="short_description">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                            </div>
                        </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <input type="submit" class="btn btn-primary" value="Submit" name="Submit" />
                    <input type="reset" class="btn btn-default" value="Reset" />
                </div>
                </form>

                <?php
                $image = "";
                $title = "";
                $short_description = "";
                $description = "";

                if (isset($_POST['Submit'])) {
                    $img = $_FILES['image']['name'];

                    $title = $_POST['title'];
                    $short_description = $_POST['short_description'];
                    $description = $_POST['description'];





                     
                    $file = $_FILES['image'];
                    $file_name_with_extenstion = $file['name'];
                    $file_name = pathinfo($file_name_with_extenstion, PATHINFO_FILENAME);
                    $extension = pathinfo($file_name_with_extenstion, PATHINFO_EXTENSION);
                    $file_tmp_location = $_FILES['image']['tmp_name'];
                    $upload_name = $file_name . time() . ".$extension";
                    if (move_uploaded_file($file_tmp_location, "uploads/" . $upload_name)) {
                        ?>

                           <div class="alert alert-success">
                           <strong>The file has been uploaded Success!</strong> .
                         </div>

                          <?php  
                    } else {
                        echo "There was an error.";
                    }
                    $result = mysqli_query($conn, "INSERT into tbl_feedback values('','uploads/" . "$upload_name','$title','$short_description','$description')");
                }
                 
                ?>
            </div>
        </div>
</div>
</section>

</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>