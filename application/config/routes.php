<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "Frontend";
$route['404_override'] = 'frontend/pageNotFound';

/*********** ADMIN ROUTES *******************/

$route['admin'] = 'admin/login/loginMe';
$route['admin/loginMe'] = 'admin/login/loginMe';
$route['admin/dashboard'] = 'admin/common_controller/index';
$route['admin/logout'] = 'admin/user/logout';
$route['admin/userListing'] = 'admin/user/userListing';
$route['admin/userListing/(:num)'] = "admin/user/userListing/$1";
$route['admin/addNew'] = "admin/user/addNew";

$route['admin/addNewUser'] = "admin/user/addNewUser";
$route['admin/editOld'] = "admin/user/editOld";
$route['admin/editOld/(:num)'] = "admin/user/editOld/$1";
$route['admin/editUser'] = "admin/user/editUser";
$route['admin/deleteUser'] = "admin/user/deleteUser";
$route['admin/loadChangePass'] = "admin/user/loadChangePass";
$route['admin/changePassword'] = "admin/user/changePassword";
$route['admin/pageNotFound'] = "admin/user/pageNotFound";
$route['admin/checkEmailExists'] = "admin/user/checkEmailExists";

$route['admin/forgotPassword'] = "admin/login/forgotPassword";
$route['admin/resetPasswordUser'] = "admin/login/resetPasswordUser";
$route['admin/resetPasswordConfirmUser'] = "admin/login/resetPasswordConfirmUser";
$route['admin/resetPasswordConfirmUser/(:any)'] = "admin/login/resetPasswordConfirmUser/$1";
$route['admin/resetPasswordConfirmUser/(:any)/(:any)'] = "admin/login/resetPasswordConfirmUser/$1/$2";
$route['admin/createPasswordUser'] = "admin/login/createPasswordUser";
$route['admin/add-additional-service'] = "admin/common_controller/add_additional_service";
$route['admin/all-additional-service'] = "admin/common_controller/all_additional_service";

$route['admin/donors'] = "admin/common_controller/donors";

$route['admin/events'] = "admin/common_controller/events";

$route['admin/list-users/(:any)'] = "admin/common_controller/list_users/$1";

$route['admin/list-service-request/(:any)'] = "admin/common_controller/list_service_request/$1";

$route['admin/new-feed'] = "admin/common_controller/new_feed";
$route['admin/add-idols'] = "admin/common_controller/add_idols";
$route['admin/add-feedback'] = "admin/common_controller/add_feedback";
$route['admin/add-teams'] = "admin/common_controller/add_teams";

$route['admin/edit-idols/(:any)'] = "admin/common_controller/edit_idols/$1";

$route['admin/edit-feedback/(:any)'] = "admin/common_controller/edit_feedback/$1";
$route['admin/edit-teams/(:any)'] = "admin/common_controller/edit_teams/$1";
$route['admin/edit-additional-service/(:any)'] = "admin/common_controller/edit_additional_service/$1";

$route['admin/edit-membership/(:any)'] = "admin/common_controller/edit_membership/$1";

$route['admin/edit-feed/(:any)'] = "admin/common_controller/edit_feed/$1";
$route['admin/all-feeds'] = "admin/common_controller/all_feeds";
$route['admin/all-teams'] = "admin/common_controller/all_teams";


$route['admin/all-idols/(:any)'] = "admin/common_controller/all_idols/$1";
$route['admin/all-idols'] = "admin/common_controller/all_idols";

$route['admin/all-feedback/(:any)'] = "admin/common_controller/all_feedback/$1";
$route['admin/all-feedback'] = "admin/common_controller/all_feedback";


$route['admin/membership/(:any)'] = "admin/common_controller/membership/$1";
$route['admin/membership'] = "admin/common_controller/membership";



$route['admin/edit-event/(:any)'] = "admin/common_controller/edit_event/$1";

 

$route['admin/view-service-request/(:any)'] = "admin/common_controller/view_service_request/$1";
$route['admin/edit-service-request/(:any)'] = "admin/common_controller/edit_service_request/$1";

$route['admin/all-cf'] = "admin/common_controller/all_cf";
$route['admin/new-cf'] = "admin/common_controller/new_cf";
$route['admin/edit-cf/(:any)'] = "admin/common_controller/edit_cf/$1";

$route['admin/insert'] = "admin/common_controller/insert";
$route['admin/insert1'] = "admin/common_controller/insert1";
$route['admin/update'] = "admin/common_controller/update";

/*********** FRONTEND ROUTES *******************/

$route['home'] = 'frontend/news';
$route['post/(:any)/(:any)'] = 'frontend/news_post/$1/$2';
$route['posts/(:any)/(:any)'] = 'frontend/news_posts/$1/$2';
$route['event/(:any)/(:any)'] = 'frontend/event/$1';

$route['login'] = 'frontend/login';
$route['register'] = 'frontend/register';
$route['loginPost'] = 'frontend/loginPost';
$route['registerPost'] = 'frontend/registerPost';
$route['logout'] = 'frontend/logout';

$route['my-account'] = 'frontend/my_account';
$route['my-profile'] = 'frontend/my_profile';
$route['update-my-profile'] = 'frontend/update_my_profile';
$route['my-profile-change-password'] = 'frontend/my_profile_change_password';

$route['profile-image-form'] = 'frontend/profile_image_form';
$route['profile-banner-image-form'] = 'frontend/profile_banner_image_form';

$route['post-delete'] = 'frontend/post_delete';

$route['add-acheivements'] = 'frontend/add_acheivements';
$route['user-gallery-images-upload'] = 'frontend/user_gallery_images_upload';

$route['about-us'] = 'frontend/about_us';
$route['fund-raisers'] = 'frontend/fund_raisers';
$route['fund-raiser/(:any)/(:any)'] = 'frontend/fund_raiser/$1';
$route['form-donate-now'] = 'frontend/form_donate_now';
$route['payment-result'] = 'frontend/payment_result';
$route['payment-result-service-request'] = 'frontend/payment_result_service_request';

$route['events'] = 'frontend/events';
$route['post-a-event'] = 'frontend/post_a_event';
$route['new-event'] = 'frontend/new_event';
$route['my-subscription'] = 'frontend/my_subscription';
$route['my-events'] = 'frontend/my_events';
$route['edit-event/(:any)/(:any)'] = 'frontend/edit_event/$1';
$route['edit-event-post'] = 'frontend/edit_event_post';

$route['services'] = 'frontend/services';
$route['apply-for-services/(:any)/(:any)'] = 'frontend/apply_for_services/$1/$2';
$route['apply-for-services'] = 'frontend/services';
$route['services/(:any)/(:any)'] = 'frontend/services1/$1/$2';

$route['paylist'] = 'frontend/paylist';
$route['subscription'] = 'frontend/subscription';

$route['service-new-request'] = 'frontend/service_new_request';

$route['profile/(:any)/(:any)'] = 'frontend/user_profile/$1';

$route['member_follow_form'] = 'frontend/member_follow_form';

